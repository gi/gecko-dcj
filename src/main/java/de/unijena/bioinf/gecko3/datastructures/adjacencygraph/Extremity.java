/*
 * Copyright 2019 Diego Rubert
 *
 * This file is part of Gecko3.
 *
 * Gecko3 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gecko3 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Gecko3.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Gene Extremity
 * @author Diego Rubert
 */

package de.unijena.bioinf.gecko3.datastructures.adjacencygraph;

import de.unijena.bioinf.gecko3.datastructures.Gene;


public class Extremity {

    private final Gene gene;      // gene names are integer numbers
    private final String family; // label of family
    private final Type type;     // TAIL or HEAD
    private final String label;  // label of adjacency
    
    private static final String tailSymbol = "\u209C";
    private static final String headSymbol = "\u2095";

    public enum Type {
        TAIL,
        HEAD
    }
    
    public Extremity(Gene gene, String family, Type type) { 
        this.gene = gene;
        this.family = family;
        this.type = type;
        this.label = family + (type == Type.TAIL ? tailSymbol : headSymbol);
    }

    /*
     * Returns true only if they are the same extremity of same gene
     * (both tail or head)
     */
    public boolean equals(Extremity other) {
        if(other == null) return false;
        else return other.family.equals(this.family) && other.type == this.type;
    }

    /*
     * Just returns the label
     */
    @Override
    public String toString() {
        return this.label;
    }

    public String getLabel() {
        return this.label;
    }

    public Extremity not() {
        return new Extremity(this.gene, this.family, this.type == Type.TAIL ? Type.HEAD : Type.TAIL);
    }
}

