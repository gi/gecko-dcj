/*
 * Copyright 2019 Diego Rubert
 *
 * This file is part of Gecko3.
 *
 * Gecko3 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gecko3 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Gecko3.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Stores results of DCJ Similarity calculation
 * @author Diego Rubert
 */

package de.unijena.bioinf.gecko3.datastructures.util;

import de.unijena.bioinf.gecko3.datastructures.Gene;
import de.unijena.bioinf.gecko3.datastructures.Subsequence;
import de.unijena.bioinf.gecko3.datastructures.adjacencygraph.AdjacencyGraph;
import de.unijena.bioinf.gecko3.datastructures.adjacencygraph.Edge;
import java.util.ArrayList;
import java.util.List;

public class DCJSimilarityData {

    private double sim;    // local DCJ similarity value
    private int discarded;       // number of discarded genes
    private AdjacencyGraph ag;   // Adjacency Graph
    private final int cycles;    // number of cycles found in decomposition (to get the cycles, ag.getDecomposition)
    private final int odd;       // number of odd paths found in decomposition
    private final int even;      // number of even paths found in decomposition
    private int startA;          // interval start (the best similarity found may be of and interval of the original subsequence), 0 means the whole subsequence (no subinterval)
    private int stopA;           // interval end
    private int startB;
    private int stopB;
    short[] genesMap;            // maps genes in B to the positions in A they were associated (-1 means none)
    
    public static DCJSimilarityData noSimilarity (Subsequence A, Subsequence B) {
        return new DCJSimilarityData(
            Double.NEGATIVE_INFINITY,
            0,
            new AdjacencyGraph(new ArrayList<Gene>(0), new ArrayList<Gene>(0), new ArrayList<String>(0), new ArrayList<String>(0), "EMPTY"),
            new ArrayList<List<Edge>>(0),
            new ArrayList<List<Edge>>(0),
            new ArrayList<List<Edge>>(0),
            A.getStart(),
            A.getStop(),
            B.getStart(),
            B.getStop(),
            null);
    }
          
    public DCJSimilarityData(double sim, int discarded, AdjacencyGraph ag, List<List<Edge>> cycles, List<List<Edge>> odd, List<List<Edge>> even, int startA, int stopA, int startB, int stopB, short[] genesMap) {
        this(sim, discarded, ag, cycles.size(), odd.size(), even.size(), startA, stopA, startB, stopB, genesMap);
        this.ag.setDecomposition(new AdjacencyGraph.Decomposition(cycles, odd, even));
    }
    
    public DCJSimilarityData(double sim, int discarded, AdjacencyGraph ag, List<List<Edge>> cycles, List<List<Edge>> odd, List<List<Edge>> even, int startA, int stopA, int startB, int stopB) {
        this(sim, discarded, ag, cycles.size(), odd.size(), even.size(), startA, stopA, startB, stopB, null);
        this.ag.setDecomposition(new AdjacencyGraph.Decomposition(cycles, odd, even));
    }
    
    public DCJSimilarityData(double sim, int discarded, AdjacencyGraph ag, List<List<Edge>> cycles, List<List<Edge>> odd, List<List<Edge>> even) {
        this(sim, discarded, ag, cycles, odd, even, 0, 0, 0, 0);
    }
    
    public DCJSimilarityData(double sim, int discarded, AdjacencyGraph ag, AdjacencyGraph.Decomposition d, int startA, int stopA, int startB, int stopB) {
        this(sim, discarded, ag, d.cycles().size(), d.odd().size(), d.even().size(), startA, stopA, startB, stopB, null);
        this.ag.setDecomposition(d);
    }
    
    public DCJSimilarityData(double sim, int discarded, AdjacencyGraph ag, AdjacencyGraph.Decomposition d, int startA, int stopA, int startB, int stopB, short[] genesMap) {
        this(sim, discarded, ag, d.cycles().size(), d.odd().size(), d.even().size(), startA, stopA, startB, stopB, genesMap);
        this.ag.setDecomposition(d);
    }
    
    public DCJSimilarityData(double sim, int discarded, AdjacencyGraph ag, AdjacencyGraph.Decomposition d) {
        this(sim, discarded, ag, d, 0, 0, 0, 0);
    }
    
    public DCJSimilarityData(double sim, int discarded, AdjacencyGraph ag, AdjacencyGraph.Decomposition d, short[] genesMap) {
        this(sim, discarded, ag, d, 0, 0, 0, 0, genesMap);
    }
    
    public DCJSimilarityData(double sim, int discarded, AdjacencyGraph ag, int cycles, int odd, int even, int startA, int stopA, int startB, int stopB, short[] genesMap) {
        this.sim = sim;
        this.discarded = discarded;
        this.ag = ag;
        this.cycles = cycles;
        this.odd = odd;
        this.even = even;
        this.startA = startA;
        this.stopA = stopA;
        this.startB = startB;
        this.stopB = stopB;
        this.genesMap = genesMap;
    }
    
    public DCJSimilarityData(double sim, int discarded, AdjacencyGraph ag, int cycles, int odd, int even) {
        this(sim, discarded, ag, cycles, odd, even, 0, 0, 0, 0, null);
    }
    
    /*
     * Returns true if the data represents no similarity
     */
    public boolean noSimilarity() {
        return Double.isInfinite(sim);
    }

    /*
     * Returns the local DCJ Similarity value
     */
    public double sim() {
        return sim;
    }

    /*
     * Sets the local DCJ Similarity value
     */
    public void setSim(double sim) {
        this.sim = sim;
    }

    /*
     * Returns the number of discarded genes
     */
    public int discarded() {
        return discarded;
    }
    
    /*
     * Sets the number of discarded genes
     */
    public void setDiscarded(int discarded) {
        this.discarded = discarded;
    }

    /*
     * Returns the adjacency graph
     */
    public AdjacencyGraph ag() {
        return ag;
    }
    
    /*
     * Sets the adjacency graph to null to save memory
     */
    public void discardAg() {
        ag = null;
    }
    
    /*
     * Sets the adjacency graph
     */
    public void setAg(AdjacencyGraph ag) {
        this.ag = ag;
    }
    
    /*
     * Sets the genesMap
     */
    public void setGenesMap(short[] genesMap) {
        this.genesMap = genesMap;
    }
    
    /*
     * Gets the genesMap
     */
    public short[] genesMap() {
        return this.genesMap;
    }
    
    /*
     * Discards the geneMap
     */
    public void discardGenesMap() {
        this.genesMap = null;
    }
    
    /*
     * Returns the number of cycles of the adjacency graph decomposition
     */
    public int cycles() {
        return cycles;
    }

    /*
     * Returns the number of odd paths of the adjacency graph decomposition
     */
    public int odd() {
        return odd;
    }

    /*
     * Returns the number of even paths of the adjacency graph decomposition
     */
    public int even() {
        return even;
    }
    
    /*
     * Returns the number of DCJ's required to sort one sequence into another
     * (that is, the DCJ-distance for sorting local regions). One of the following
     * must be set properly for this method computing the correct value:
     *   - Adjacency graph
     *   - startA, endA, startB, endB, discarded
     * Returns Integer.MAX_VALUE if there are no similarity between sequences
     * (that is, the sequences have no gene in common).
     */
    public int DCJsToSort() {
        if (noSimilarity())
            return Integer.MAX_VALUE;
        
        int n;
        if (ag != null)
            n = ag.size()/2 - 1;
        else
            n = (stopA - startA + 1 + stopB - startB + 1 - discarded) / 2;
        return n  - cycles - odd/2;
    }
    
    /*
     * Returns the interval start of sequence A for which this similarity was computed,
     * 0 means the whole subsequence (no subinterval)
     */
    public int startA() {
        return startA;
    }
    
    /*
     * Returns the interval stop of sequence A for which this similarity was computed,
     * 0 means the whole subsequence (no subinterval)
     */
    public int stopA() {
        return stopA;
    }
    
    /*
     * Returns the interval start of sequence B for which this similarity was computed,
     * 0 means the whole subsequence (no subinterval)
     */
    public int startB() {
        return startB;
    }
    
    /*
     * Returns the interval stop of sequence B for which this similarity was computed,
     * 0 means the whole subsequence (no subinterval)
     */
    public int stopB() {
        return stopB;
    }

    /*
     * Sets the interval start and stop of sequence A for which this similarity was computed
     */
    public void setStartStopA(int startA, int stopA) {
        this.startA = startA;
        this.stopA = stopA;
    }
    
    /*
     * Sets the interval start and stop of sequence B for which this similarity was computed
     */
    public void setStartStopB(int startB, int stopB) {
        this.startB = startB;
        this.stopB = stopB;
    }
    
    /*
     * Sets the intervals start and stop for which this similarity was computed
     */
    public void setStartStop(int startA, int stopA, int startB, int stopB) {
        this.startA = startA;
        this.stopA = stopA;
        this.startB = startB;
        this.stopB = stopB;
    }

    /*
     * Returns a string displaying main fields
     */
    @Override
    public String toString() {
        String str;
        
        str = String.format("DCJ similarity = %g ", sim);
        str += String.format("(refseq. interval [%d-%d], ", startA, stopA);
        str += String.format("this seq. interval [%d-%d])", startB, stopB);
        str += String.format(", discarded genes = %d", discarded);
        str += String.format(", cycles = %d", cycles);
        str += String.format(", odd paths = %d", odd);
        str += String.format(", even paths = %d", even);
        str += String.format(", DCJs to sort = %d", DCJsToSort());
        
        return str;
    }
    
    /*
     * Returns a string displaying main fields
     */
    public String toString(boolean computeSubintervals) {
        String str;
        
        if (!computeSubintervals)
            str = String.format("DCJ similarity = %g", sim);
        else { // compute subintervals is on
            str = String.format("Best DCJ similarity = %g ", sim);
            str += String.format("(refseq. subinterval [%d-%d], ", startA, stopA);
            str += String.format("this seq. subinterval [%d-%d])", startB, stopB);
        }
        str += String.format(", discarded genes = %d", discarded);
        str += String.format(", cycles = %d", cycles);
        str += String.format(", odd paths = %d", odd);
        str += String.format(", even paths = %d", even);
        str += String.format(", DCJs to sort = %d", DCJsToSort());
        
        if (genesMap != null && genesMap.length > 0) {
            str += ", refseq positions = [" + genesMap[0];
            for (int idx = 1; idx < genesMap.length; ++idx)
                str += "," + genesMap[idx];
            str += "]";
        }
        //str += ", refseq positions = " + Arrays.toString(genesMap);
        
        return str;
    }
    
}
