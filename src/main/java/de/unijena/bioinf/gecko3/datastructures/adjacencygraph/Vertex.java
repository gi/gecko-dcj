/*
 * Copyright 2019 Diego Rubert
 *
 * This file is part of Gecko3.
 *
 * Gecko3 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gecko3 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Gecko3.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Adjacency Graph Vertex
 * @author Diego Rubert
 */

package de.unijena.bioinf.gecko3.datastructures.adjacencygraph;

import java.util.ArrayList;
import java.util.List;


public class Vertex {

    private final String label;
    private final Extremity ex[]; // Stores the two extremities of a vertex
    private final char part; // Used to determine which part a vertex belongs to (should be A or B) and to distinguish nodes with same label in the graph drawing
    private int idx; // We assume each vertex will share two adjacencies of *genes* with indexes idx-1 (if >= 0) and idx (if < n).
    private List<Edge> edges;


    /*
     * We assume no circular chromosome with just 1 gene (e.g. only one extremity 2t2h)
     */
    public Vertex(Extremity ex1, Extremity ex2, char part) {
        if (part != 'A' && part != 'B')
            throw new IllegalArgumentException​("Invalid part: vertices in adjacency graph must belong to parts 'A' or 'B'.");
        String ex1l = (ex1 == null) ? "" : ex1.toString();
        String ex2l = (ex2 == null) ? "" : ex2.toString();
        this.label = ex1l + ex2l;
        this.ex = new Extremity[2];
        this.ex[0] = ex1;
        this.ex[1] = ex2;
        this.part = part;
        this.idx = -1; // No idx for now
        this.edges = new ArrayList<>(2);
    }

    public void setIdx(int idx) {
        this.idx = idx;
    }

    @Override
    public String toString() {
        return this.label + this.part;
    }

    public String getLabel() {
        return label;
    }

    public int degree() {
        return edges.size();
    }

    public void addEdge(Vertex v, String label) {
        edges.add(new Edge(v, label));
    }

    public List<Edge> getEdges() {
        return edges;
    }

    public Extremity[] getExtremities() {
        return ex;
    }
}
