/*
 * Copyright 2019 Diego Rubert
 *
 * This file is part of Gecko3.
 *
 * Gecko3 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gecko3 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Gecko3.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Adjacency Graph for genomes without duplicate genes
 * @author Diego Rubert
 */

package de.unijena.bioinf.gecko3.datastructures.adjacencygraph;

import de.unijena.bioinf.gecko3.datastructures.Gene;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class AdjacencyGraph {

    private String label;
    private List<Vertex> verticesA;      // List of adjacencies (we have parts A and B in the adjacency graph)
    private List<Vertex> verticesB;      // This is NOT a list of genes
    private Decomposition decomposition; // Stores one decomposition of the adjacency graph

    /*
     * Receives 2 lists of genes, each list defines a sequence of genes in a
     * chromosome in one genome. Also needs 2 lists of strings representing
     * families (labels) of A and B, in the same order of the lists of genes.
     * The lists of families wich will be used to identify which gene of A must
     * be associated to which gene of B. Since this Adjacency Graph supports
     * only genomes without duplicate genes, the families must be an exact one 
     * to one mapping (de-duplication can be done previously).
     */
    public AdjacencyGraph(List<Gene> A, List<Gene> B, List<String> familiesA, List<String> familiesB) {
        this(A, B, familiesA, familiesB, "");
    }

    public AdjacencyGraph(List<Gene> A, List<Gene> B, List<String> familiesA, List<String> familiesB, String label) {
        this.label = label;
        this.verticesA = new ArrayList<>(A.size() + 2); // A.size() genes = A.size()+1 adjacencies
        this.verticesB = new ArrayList<>(B.size() + 2); // A.size() genes = A.size()+1 adjacencies
        this.decomposition = null;

        List<Extremity> adjacencies = generateAdjacencies(A, familiesA);
        for (int i = 0; i < adjacencies.size(); i += 2)
            addVertex(new Vertex(adjacencies.get(i), adjacencies.get(i+1), 'A'), 'A');

        adjacencies = generateAdjacencies(B, familiesB);
        for (int i = 0; i < adjacencies.size(); i += 2)
            addVertex(new Vertex(adjacencies.get(i), adjacencies.get(i+1), 'B'), 'B');

        // For genomes with duplicates we would need the labels/ext in edges
        // but we "de"-duplicate genes before getting here (and thus later we
        // will have just one decomposition of the adjacency graph

        // hash with the lists of vertices with some extremity
        // we have 2 * A.size() extremities, then load factor = 0.5
        HashMap<String, List<Vertex>> map = new HashMap<>( (int) (4 * A.size()) );

        for (Vertex v : verticesB)
            for (Extremity ex : v.getExtremities())
                if (ex != null) {
                    String exlabel = ex.getLabel();
                    List<Vertex> vlist = map.get(exlabel);
                    if (vlist == null) {
                        map.put(exlabel, new ArrayList<Vertex>(2));
                        vlist = map.get(exlabel);
                    }
                    vlist.add(v);
                }

        for (Vertex u : verticesA)
            for (Extremity ex : u.getExtremities())
                if (ex != null) {
                    String exlabel = ex.getLabel();
                    List<Vertex> vlist = map.get(exlabel);
                    if (vlist != null)
                        for (Vertex v : vlist)
                            addEdge(u, v, exlabel);
                }

    }

    private List<Extremity> generateAdjacencies(List<Gene> genome, List<String> families) {
        List<Extremity> adj = new ArrayList<>(2 * genome.size() + 4);
        Extremity ex1, ex2;
        
        if (genome.isEmpty())
            return adj;

        Gene g = genome.get(0);
        String f = families.get(0);
        ex2 = new Extremity(g, f, g.getOrientation().equals(Gene.GeneOrientation.POSITIVE) ? Extremity.Type.TAIL : Extremity.Type.HEAD);
        adj.add(null);
        adj.add(ex2);

        for (int i = 1; i < genome.size(); i++) {
            g = genome.get(i);
            f = families.get(i);
            ex1 = ex2.not();
            ex2 = new Extremity(g, f, g.getOrientation().equals(Gene.GeneOrientation.POSITIVE) ? Extremity.Type.TAIL : Extremity.Type.HEAD);
            adj.add(ex1);
            adj.add(ex2);
        }

        ex1 = ex2.not();
        adj.add(ex1);
        adj.add(null);
        return adj;
    }

    private void addVertex(Vertex v, char part) {
        if (part == 'A') {
            v.setIdx(verticesA.size());
            verticesA.add(v);
        }
        else {
            v.setIdx(verticesB.size());
            verticesB.add(v);
        }
    }

    private void addEdge(Vertex u, Vertex v, String label) {
        u.addEdge(v, label);
        v.addEdge(u, label);
    }

    public List<Vertex> getVerticesA() {
        return verticesA;
    }

    public List<Vertex> getVerticesB() {
        return verticesB;
    }

    public List<Vertex> getVertices() {
        List<Vertex> vertices = new ArrayList<>(verticesA);
        vertices.addAll(verticesB);
        return vertices;
    }

    @Override
    public String toString() {
        List<Vertex> allVertices = new ArrayList<>(verticesA.size() + verticesB.size());
        allVertices.addAll(verticesA);
        allVertices.addAll(verticesB);

        String s = "Adjacency Graph " + label + "\n";
        for (Vertex v : allVertices) {
            s += v + ": ";
            List<Edge> edges = v.getEdges();
            List<String> elabels = new ArrayList<>(edges.size());
            for (Edge e : edges)
                elabels.add(e.toString()); // when printing the edges, just the other endpoint of the edge is shown
            s += String.join(", ", elabels) + "\n";
        }
        return s;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int size() {
        return verticesA.size() + verticesB.size();
    }

    public Decomposition getDecomposition() {
        return decomposition;
    }

    public void setDecomposition(List<List<Edge>> cycles, List<List<Edge>> odd, List<List<Edge>> even) {
        this.setDecomposition(new Decomposition(cycles, odd, even));
    }
    
    public void setDecomposition(Decomposition decomposition) {
        this.decomposition = decomposition;
    }
    
    
    /*
     * Saves a decomposition of some adjacency graph
     */
    public static class Decomposition {
        private final List<List<Edge>> cycles; // cycles found in decomposition
        private final List<List<Edge>> odd;    // odd paths found in decomposition
        private final List<List<Edge>> even;   // even paths found in decomposition

        /*
         * Default constructor
         */
        public Decomposition(List<List<Edge>> cycles, List<List<Edge>> odd, List<List<Edge>> even) {
            this.cycles = cycles;
            this.odd = odd;
            this.even = even;
        }

        /*
         * Returns the set of cycles
         */
        public List<List<Edge>> cycles() {
            return cycles;
        }

        /*
         * Returns the set of odd paths
         */
        public List<List<Edge>> odd() {
            return odd;
        }

        /*
         * Returns the set of even paths
         */
        public List<List<Edge>> even() {
            return even;
        }
    }
}
