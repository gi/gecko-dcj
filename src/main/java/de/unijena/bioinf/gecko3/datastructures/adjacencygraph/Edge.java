/*
 * Copyright 2019 Diego Rubert
 *
 * This file is part of Gecko3.
 *
 * Gecko3 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gecko3 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Gecko3.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Adjacency Graph Edge
 * @author Diego Rubert
 */

package de.unijena.bioinf.gecko3.datastructures.adjacencygraph;


public class Edge {
    private final Vertex to;
    private final String label;

    public Edge(Vertex to, String label) {
        this.to = to;
        this.label = label;
    }

    public Vertex to() {
        return this.to;
    }

    public String getLabel() {
        return this.label;
    }

    /*
     * This returns just one of the endpoints of the edge
     */
    @Override
    public String toString() {
        return to + "(" + label + ")";
    }
}

