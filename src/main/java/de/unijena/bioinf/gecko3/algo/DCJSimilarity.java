/*
 * Copyright 2019 Diego Rubert
 *
 * This file is part of Gecko3.
 *
 * Gecko3 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gecko3 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Gecko3.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Computes de local DCJ similarity for a pair of sequences (the object stores
 * the parameters and the task, and calculates the local DCJ similarity for passed
 * sequences in computeDCJSimilarity* methods)
 * @author Diego Rubert
 */

package de.unijena.bioinf.gecko3.algo;

import de.unijena.bioinf.gecko3.datastructures.Subsequence;
import de.unijena.bioinf.gecko3.datastructures.Chromosome;
import de.unijena.bioinf.gecko3.datastructures.Gene;
import de.unijena.bioinf.gecko3.datastructures.GeneFamily;
import de.unijena.bioinf.gecko3.datastructures.Parameter;
import de.unijena.bioinf.gecko3.datastructures.adjacencygraph.AdjacencyGraph;
import de.unijena.bioinf.gecko3.datastructures.adjacencygraph.Edge;
import de.unijena.bioinf.gecko3.datastructures.adjacencygraph.Vertex;
import de.unijena.bioinf.gecko3.datastructures.util.DCJSimilarityData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import javax.swing.SwingWorker;

/*
 * Implements the local DCJ similarity calculation and algorithms necessary to the calculation
 */
public class DCJSimilarity {
    
    private final Parameter parameters;
    private final SwingWorker<Void, String> task;
    
    /*
     * Constructor for computing dcj similarity in a task
     */
    public DCJSimilarity(Parameter parameters, SwingWorker<Void, String> task) {
        this.parameters = parameters;
        this.task = task;
    }

    /*
     * Constructor for computing dcj similarity not in a task
     */
    public DCJSimilarity(Parameter parameters) {
        this(parameters, null);
    }

    /*
     * Given two subsequences, computes the local DCJ similarity, interrupting
     * the calculation if the task is canceled
     */
    public DCJSimilarityData computeDCJSimilarity(Subsequence seqA, Chromosome chrA, Subsequence seqB, Chromosome chrB) throws InterruptedException {
        List<Gene> A, A_unknown;
        List<Gene> B, B_unknown;
        DCJSimilarityData dcj;
        int extend = parameters.computeSubintervals() ? parameters.getExtendSubsequences() : 0;

        // First we get genomes with all genes (with families + unknown families 0)
        A_unknown = getExtendedSequenceGenes(seqA, chrA, extend);
        B_unknown = getExtendedSequenceGenes(seqB, chrB, extend);
        
        // this would be done when computing clean sequences and families, but it is
        // more efficient to do this before computing subintervals, deduplicating, etc
        A = removeUnknown(A_unknown);
        B = removeUnknown(B_unknown);
        int discarded_unknown = A_unknown.size() - A.size() + B_unknown.size() - B.size();

        // if seqA == seqB, we don't compute the similarity for subintervals, because we know the whole interval will give the best score
        if (parameters.computeSubintervals() && seqA != seqB)
            dcj = computeDCJSimilarityAllSubintervals(A, B);
        else {
            DeduplicationDCJ dedup = new DeduplicationDCJ(A, B);
            dcj = dedup.computeDCJSimilarity();
        }
        
        if (dcj != null) {
            dcj.setDiscarded(dcj.discarded() + discarded_unknown);
            dcj.setSim(dcj.sim() - discarded_unknown * parameters.getDiscardedPenality());
            if (parameters.computeSubintervals() && seqA != seqB) { // setting correct subintervals ranges in dcj results data
                setActualStartStop(dcj, seqA.getStart(), extend, true);
                setActualStartStop(dcj, seqB.getStart(), extend, false);
            }
            else
                dcj.setStartStop(seqA.getStart(), seqA.getStop(), seqB.getStart(), seqB.getStop());
        }

        return dcj;
    }
    
    /*
     * Given two subsequences, computes the local DCJ similarity for all subintervals,
     * interrupting the calculation if the task is canceled
     */
    private DCJSimilarityData computeDCJSimilarityAllSubintervals(List<Gene> A, List<Gene> B) throws InterruptedException {
        // these arrays hold initially the original labels of families, and later
        // will hold the "new" label of deduplicated families (for families with duplicates)
        List<String> familiesA = new ArrayList<>(A.size());
        List<String> familiesB = new ArrayList<>(B.size());
        for (Gene g : A)
            familiesA.add(g.getExternalId());
        for (Gene g : B)
            familiesB.add(g.getExternalId());
        
        LinkedList<Integer> occA, occB;
        GeneFamily family;
        
        DCJSimilarityData dcj;        
        DCJSimilarityData best = null;
        
        // A quick way to optimize A intervals: if the beginning or the end of
        // the interval in A is a gene not present in B, this interval is
        // surely not optimal, then we can skip it
        HashSet<GeneFamily> setFamiliesB = new HashSet<>(2 * B.size());
        for (Gene g : B)
            setFamiliesB.add(g.getGeneFamily());
        
        
        for (int i = 0; i < A.size(); i++) {
            
            // small optimization
            if (!setFamiliesB.contains(A.get(i).getGeneFamily()))
                continue;
            
            // we keep track of family occorrences in the current interval
            HashMap<GeneFamily,LinkedList<Integer>> mapA = emptyFamilyOccurrencesMap(A);
            
            for (int j = i; j < A.size(); j++) {
                family = A.get(j).getGeneFamily();
                mapA.get(family).add(j - i);
                
                // small optimization
                if (!setFamiliesB.contains(family))
                    continue;
                
                List<Gene> iA = A.subList(i, j + 1);
                List<String> iFamiliesA = familiesA.subList(i, j + 1);

                for (int k = 0; k < B.size(); k++) {
                    
                    // small optimization
                    // if the gene in the beginning of the interval of B (position k)
                    // is not in the interval of A, we don't compute the similarity,
                    // because there will be a better score when the interval in B
                    // doesn't include the gene in position k
                    occA = mapA.get(B.get(k).getGeneFamily());
                    if (occA == null || occA.isEmpty()) // A doesn't contain this family or the whole A contains but not the current [i,j] interval
                        continue;
                    
                    HashMap<GeneFamily,LinkedList<Integer>> mapB = emptyFamilyOccurrencesMap(B);
                    
                    // we keep track of which families have duplicates for the current interval
                    LinkedList<GeneFamily> familiesWithDuplicates = new LinkedList<>();
        
                    for (int l = k; l < B.size(); l++) {
                        
                        if (task != null && task.isCancelled()) throw new InterruptedException();
                        
                        family = B.get(l).getGeneFamily();
                        occA = mapA.get(family);
                        occB = mapB.get(family);
                        occB.add(l - k);
                        
                        // small optimization as before, but for the end of the interval of B
                        if (occA == null || occA.isEmpty()) // A doesn't contain this family or the whole A contains but not the current [i,j] interval
                            continue;
                        
                        //if (occA != null && occA.size() > 0 && occA.size() + occB.size() > 2)
                        if (occA.size() + occB.size() > 2)
                            familiesWithDuplicates.add(family);
                        
                        List<Gene> iB = B.subList(k, l + 1);
                        List<String> iFamiliesB = familiesB.subList(k, l + 1);
                        
                        DeduplicationDCJ dedup = new DeduplicationDCJ(iA, iFamiliesA, mapA, iB, iFamiliesB, mapB, familiesWithDuplicates, parameters.useHeuristics());
                        dcj = dedup.computeDCJSimilarity();
                        best = bestDCJSimilarityResult(best, dcj);

                        if (best != null && best == dcj) // we have updated the best
                            best.setStartStop(i, j, k, l);
                    }
                }
            }
        }
        
        return best;
    }
    
    /*
     * Computes the DCJ similarity for two sequences (after family deduplication)
     */
    private DCJSimilarityData computeDCJSimilarity(List<Gene> A, List<String> familiesA, List<Gene> B, List<String> familiesB) {
        double sim;    // similarity score
        int discarded; // number of discarded genes
        
        Set<String> commonFamilies = commonStringFamilies(familiesA, familiesB);
        if (commonFamilies.isEmpty())
            return null;
                
        ArrayList<Gene> cleanA = new ArrayList<>(A.size());
        ArrayList<String> cleanFamiliesA = new ArrayList<>(A.size());
        computeCleanSequencesAndFamilies(A, familiesA, commonFamilies, cleanA, cleanFamiliesA);
        discarded = A.size() - cleanA.size();
        
        List<Gene> cleanB = new ArrayList<>(B.size());
        List<String> cleanFamiliesB = new ArrayList<>(B.size());
        computeCleanSequencesAndFamilies(B, familiesB, commonFamilies, cleanB, cleanFamiliesB);
        discarded += B.size() - cleanB.size();
        
        //System.out.println("common_families:\n" +  commonFamilies);
        //System.out.println("cleanA:\n" +  cleanA);
        //System.out.println("cleanFamiliesA:\n" +  cleanFamiliesA);
        //System.out.println("cleanB:\n" +  cleanB);
        //System.out.println("cleanFamiliesB:\n" +  cleanFamiliesB);
        
        AdjacencyGraph ag = new AdjacencyGraph(cleanA, cleanB, cleanFamiliesA, cleanFamiliesB, "Clean genomes");
        //System.out.println(ag);
        
        AdjacencyGraph.Decomposition d = decomposeCyclesAndPaths(ag);
        sim = calculateSimilarityScore(d, discarded);
                
        return new DCJSimilarityData(sim, discarded, ag, d, computeGenesMap(familiesA, familiesB));
    }
    
    /*
     * Given two subsequences, computes the local DCJ similarity for an subinterval,
     * interrupting the calculation if the task is canceled
     */
    public DCJSimilarityData computeDCJSimilaritySubInterval(Subsequence seqA, Chromosome chrA, int startA, int stopA, Subsequence seqB, Chromosome chrB, int startB, int stopB) throws InterruptedException {
        List<Gene> A;
        List<Gene> B;
        DCJSimilarityData dcj;

        A = getSubSequenceGenes(seqA, chrA, startA, stopA);
        B = getSubSequenceGenes(seqB, chrB, startB, stopB);

        DeduplicationDCJ dedup = new DeduplicationDCJ(A, B);
        dcj = dedup.computeDCJSimilarity();
        
        if (dcj != null)
            dcj.setStartStop(startA, stopA, startB, stopB);

        return dcj;
    }
    
    /*
     * Returns an empty hashmap for storing for each family the positions it occurs
     */
    private HashMap<GeneFamily,LinkedList<Integer>> emptyFamilyOccurrencesMap(List<Gene> genome) {
        HashMap<GeneFamily,LinkedList<Integer>> map = new HashMap<>( (int) (2 * genome.size()) );

        for (Gene g : genome) {
            if (map.get(g.getGeneFamily()) == null)
                map.put(g.getGeneFamily(), new LinkedList<Integer>());
        }
        return map;
    }
        
    /*
     * Given two DCJ similarity results (possibly null), returnts the best one
     */
    private static DCJSimilarityData bestDCJSimilarityResult(DCJSimilarityData a, DCJSimilarityData b) {
        if (a == null)
            return b;
        if (b == null)
            return a;
        if (a.sim() > b.sim())
            return a;
        else
            return b;
    }
    
    /*
     * Computes the mapping that identifies for each gene/position in B the gene/position in A it is associated
     */
    private static short[] computeGenesMap(List<String> familiesA, List<String> familiesB) {
        short[] map = new short [familiesB.size()];
        int idx = 0;
        for (String gene : familiesB)
            map[idx++] = (short) (gene == null ? -1 : familiesA.indexOf(gene)); // -1 if doesn't find
        return map;
    }

            
    /*
     * Returns a set of familes common to A and B
     */
    private static Set<String> commonStringFamilies(List<String> familiesA, List<String> familiesB) {
        Set<String> commonFamilies = new HashSet<>(2 * familiesA.size()); // setA, load factor = 0.5
        for (String f : familiesA) if (f != null) commonFamilies.add(f); // we can't allow unknown families in the clean genomes
        
        Set<String> tempSetB = new HashSet<>(2 * familiesB.size());
        for (String f : familiesB) if (f != null) tempSetB.add(f);
        
        commonFamilies.retainAll(tempSetB); // now contains the intersection = genes in common to both
        
        return commonFamilies;
    }
    
    /*
     * Returns a set of familes common to A and B
     */
    private static Set<GeneFamily> commonFamilies(List<Gene> A, List<Gene> B) {
        Set<GeneFamily> commonFamilies = new HashSet<>(2 * A.size()); // setA, load factor = 0.5
        for (Gene i : A) if (!i.getGeneFamily().isUnknownGeneFamily()) commonFamilies.add(i.getGeneFamily()); // we can't allow unknown families in the clean genomes
        
        Set<GeneFamily> tempSetB = new HashSet<>(2 * B.size());
        for (Gene i : B) if (!i.getGeneFamily().isUnknownGeneFamily()) tempSetB.add(i.getGeneFamily());
        
        commonFamilies.retainAll(tempSetB); // now contains the intersection = genes in common to both
        
        return commonFamilies;
    }
    
    /*
     * Returns a sublist of genes in an extended interval
     */
    private static List<Gene> getExtendedSequenceGenes(Subsequence seq, Chromosome chr, int extend) {
        int start = seq.getStart() - 1 - extend; // subsequence indexes start at 1, but list indexes start at 0
        int stop = seq.getStop() + extend; // we don't subtract 1 because stop is exclusive in subList range
        int size = chr.getGenes().size();
        return chr.getGenes().subList(start >= 0 ? start : 0, stop <= size ? stop : size); 
    }
    
    /*
     * Returns a new list without genes with unknown family
     */
    private static List<Gene> removeUnknown(List<Gene> genes) {
        List<Gene> newGeneList = new ArrayList<>(genes.size());
        for (Gene g : genes)
            // the problem with !g.isUnknown() (and why we can't use it) is that
            // g.isUnknown() returns true also if the family is not 0 but contains
            // only one member, and this would remove such gene even when comparing
            // the sequence to itself
            if (!g.getGeneFamily().isUnknownGeneFamily()) 
                newGeneList.add(g);
        return newGeneList;
    }
    
    /*
     * Returns a sublist of genes in an extended interval
     */
    private static List<Gene> getSubSequenceGenes(Subsequence seq, Chromosome chr, int start, int stop) {
        // subsequence indexes start at 1, but list indexes start at 0
        // we don't subtract 1 of stop because stop is exclusive in subList range
        start -= 1; 
        int size = chr.getGenes().size();
        return chr.getGenes().subList(start >= 0 ? start : 0, stop <= size ? stop : size); 
    }
    
    /*
     * Sets the real start/stop considering subsequence indexes and extend value
     */
    private static void setActualStartStop(DCJSimilarityData dcj, int originalStart, int extend, boolean isRefSeq) {
        int start = originalStart - extend;
        start = start >= 1 ? start : 1; // actual start with extension of the original subsequence
        
        // actual start and stop of the current subinterval of the original subsequence
        int stop = start + (isRefSeq ? dcj.stopA() : dcj.stopB());
        start += (isRefSeq ? dcj.startA() : dcj.startB());
        
        if (isRefSeq)
            dcj.setStartStopA(start, stop);
        else
            dcj.setStartStopB(start, stop);
    }
    
    /*
     * Computes the DCJ similarity for two sequences (when is assured that the
     * input has no duplicate genes and is balanced). The starts and ends of the
     * DCJSimilarityData are not set.
     */
    private DCJSimilarityData computeDCJSimilarityWithoutDuplicates(List<Gene> A, List<Gene> B) {
        double sim;    // similarity score
        int discarded; // number of discarded genes
        
        Set<GeneFamily> commonFamilies = commonFamilies(A, B);
        if (commonFamilies.isEmpty())
            return null;
                
        ArrayList<Gene> cleanA = new ArrayList<>(A.size());
        ArrayList<String> cleanFamiliesA = new ArrayList<>(A.size());
        computeCleanSequencesAndFamilies(A, commonFamilies, cleanA, cleanFamiliesA);
        discarded = A.size() - cleanA.size();
        
        List<Gene> cleanB = new ArrayList<>(B.size());
        List<String> cleanFamiliesB = new ArrayList<>(B.size());
        computeCleanSequencesAndFamilies(B, commonFamilies, cleanB, cleanFamiliesB);
        discarded += B.size() - cleanB.size();
        
        //System.out.println("common_families:\n" +  commonFamilies);
        //System.out.println("cleanA:\n" +  cleanA);
        //System.out.println("cleanFamiliesA:\n" +  cleanFamiliesA);
        //System.out.println("cleanB:\n" +  cleanB);
        //System.out.println("cleanFamiliesB:\n" +  cleanFamiliesB);
        
        AdjacencyGraph ag = new AdjacencyGraph(cleanA, cleanB, cleanFamiliesA, cleanFamiliesB, "Clean genomes");
        //System.out.println(ag);
        
        AdjacencyGraph.Decomposition d = decomposeCyclesAndPaths(ag);
        sim = calculateSimilarityScore(d, discarded);
        
        return new DCJSimilarityData(sim, discarded, ag, d);
    }
    
    /*
     * Replaces clean and families with a clean sequence and the respective
     * family labels according to sequence and commonFamilies
     */
    private static void computeCleanSequencesAndFamilies(List<Gene> sequence, Set<GeneFamily> commonFamilies, List<Gene> clean, List<String> families) {
        clean.clear();
        families.clear();
        for (Gene i : sequence)
            if (commonFamilies.contains(i.getGeneFamily())) {
                clean.add(i);
                families.add(i.getExternalId());
            }  
    }
    
    /*
     * Given an array of strings representing families of genes,
     * removes nulls, which represent unassociated genes (discarded)
     */
    private void computeCleanSequencesAndFamilies(List<Gene> sequence, List<String> families, Set<String> commonFamilies, List<Gene> clean, List<String> cleanFamilies) {
        clean.clear();
        cleanFamilies.clear();
        for (int i = 0; i < sequence.size(); i++)
            if (commonFamilies.contains(families.get(i)) && !sequence.get(i).getGeneFamily().isUnknownGeneFamily()) {
                clean.add(sequence.get(i));
                cleanFamilies.add(families.get(i));
            }  
    }

    /* 
     * Calculates the score for one cycle
     */
    private double score(int length) {
        return (2 - length) / (double) (parameters.getBorderlineLength() - 2) + 1;
    }
    
    /*
     * Calculates the similarity score given a set of cycles,
     * paths and the number of discarded genes (class variables)
     */
    private double calculateSimilarityScore(AdjacencyGraph.Decomposition d, int discarded) {
        double sim = 0;
        
        for (List<Edge> c : d.cycles())
            sim += score(c.size());
        for (List<Edge> o : d.odd())
            sim += score(o.size() + 1) / 2.0;
        for (List<Edge> e : d.even())
            sim += score(e.size() + 2) / 2.0;
        sim -= discarded * parameters.getDiscardedPenality();
        return sim;
    }
    
    /*
     * Finds the (unique) decomposition of cycles and paths of an adjacency graph
     */
    private static AdjacencyGraph.Decomposition decomposeCyclesAndPaths(AdjacencyGraph ag) {
        List<List<Edge>> cycles = new LinkedList<>(); // cycles found
        List<List<Edge>> odd = new LinkedList<>();    // odd paths found
        List<List<Edge>> even = new LinkedList<>();   // even paths found

        List<Vertex> vA = ag.getVerticesA();               
        HashSet<Vertex> visited = new HashSet<>( (int) (4 * vA.size()) );
        List<Edge> cycle;

        for (Vertex v : vA) {
            if (visited.contains(v))
                continue;
            
            cycle = findCycleOrPath(v);
            if (isCycle(cycle))
                cycles.add(cycle);
            else // path
                if (cycle.size() % 2 == 0)
                    even.add(cycle);
                else
                    odd.add(cycle);
            
            for (Edge e : cycle)
                visited.add(e.to());
            if (!isCycle(cycle)) // because in a path, the first vertex isn't in any "e.to"
                visited.add(firstVertexOfPath(cycle));
        }
        
        return new AdjacencyGraph.Decomposition(cycles, odd, even);
    }
    
    /*
     * Finds the cycle or path that v belongs to (we assume an unique decomposition of ag)
     */
    private static List<Edge> findCycleOrPath(Vertex v) {

        if (v.degree() == 1)
            return findPath(v);
        
        Edge e;
        Vertex w = v;
        List<Edge> path = new LinkedList<>();

        // We have to check by label because each endpoint of an edge stores its
        // own (different) edge object (and we also may have parallel edges in
        // case of cycles of length 2)        
        String lastEdgeLabel = "";
        
        do {
            e = w.getEdges().get(0);
            if (e.getLabel().equals(lastEdgeLabel)) // we don't want to go back in the path
                e = w.getEdges().get(1);
            w = e.to();
        
            if (w.degree() == 1)
                return findPath(w);
            
            path.add(e);
            lastEdgeLabel = e.getLabel();
        } while (w != v);
        
        return path;
    }
    
    /*
     * Given one vertex of degree 1 (contains a telomere), extends the path while possible
     */
    private static List<Edge> findPath(Vertex leaf) {
        Edge e;
        Vertex w = leaf;
        List<Edge> path = new LinkedList<>();

        // We have to check by label because each endpoint of an edge stores its
        // own (different) edge object (and we also may have parallel edges in
        // case of cycles of length 2)        
        String lastEdgeLabel = "";
        
        do {
            e = w.getEdges().get(0);
            if (e.getLabel().equals(lastEdgeLabel)) // we don't want to go back in the path
                e = w.getEdges().get(1);
            w = e.to();
            
            path.add(e);
            lastEdgeLabel = e.getLabel();
        } while (w.degree() != 1);
        
        return path;
    }
    
    /*
     * Returns true if the sequence of edges is a cycle
     */
    private static boolean isCycle(List<Edge> path) {
        // A sequence of edges is a path iff it starts in an adjacency
        // containing a telomere, and in this case it will also end in an
        // adjacency containing a telomere
        // The easier way to check this is check if the last vertex has
        // degree 1 (or the first, but the first is not stored explicitly
        // in the edge set of the path)
        return path.get(path.size() - 1).to().degree() != 1;
    }
    
    /*
     * We may need this because a list of edges doesn't store explicitly which one
     * is the first vertex of a path (because for saving space, edges just store
     * one endpoint), we don't have this problem if the path forms a cycle
     */
    private static Vertex firstVertexOfPath(List<Edge> path) {
        Vertex u = path.get(0).to(), v, w;
        List<Edge> edges = u.getEdges();
        
        // this vertex has at most 2 edges, and the vertex we are looking for is
        // the one that is not the next in *path*
        v = edges.get(0).to();
        if (edges.size() == 1) // path of length 1, then its the only neighbor (we also could check path.size() == 1
            return v;
        
        w = edges.get(1).to();
        if (v == path.get(1).to())
            return w;
        else
            return v;
    }
    
    /*
     * Prints the vertices of some path/cycle
     */
    private static void printCycleOrPath(List<Edge> path) {
        System.out.println(firstVertexOfPath(path));
        for (Edge e : path)
            System.out.println(e.to());
    }
    
    
    
    
    /*
     * Class to compute heuristics and deduplication
     */
    private class DeduplicationDCJ {
	        
        private final List<Gene> A; // Genes in A
        private final List<Gene> B;
        private final HashMap<GeneFamily,LinkedList<Integer>> occMapA; // maos for each family positions they occur in A
        private final HashMap<GeneFamily,LinkedList<Integer>> occMapB;
        private final LinkedList<GeneFamily> familiesWithDuplicates; // self-explanatory, right?
        
        // these arrays hold initially the original labels of families, and later
        // will hold the "new" label  (not strand) of deduplicated families (for
        // families with duplicates)
        private final List<String> familiesA;
        private final List<String> familiesB;
        
        private int fixed; // number of genes fixed **by the heuristic only**
        
        // The maximum number of ways of associating ny "brute force" the multiple
        // copies of genes in compared sequences when using heuristics. If greater
        // that that after main heuristics are finished, we'll associate copies
        // randomly before proceeding to brute force deduplication (that computes
        // all possible associations) to keep running time under control.
        private static final int MAX_COMBINATIONS = 1024;
        
        DeduplicationDCJ(List<Gene> A, List<Gene> B) {
            this.A = A;
            this.B = B;
            
            occMapA = familyOccurrencesMap(A);
            occMapB = familyOccurrencesMap(B);
            familiesWithDuplicates = new LinkedList<>(); // Set
            familiesA = new ArrayList<>(A.size());
            familiesB = new ArrayList<>(B.size());
            for (Gene g : A)
                familiesA.add(g.getExternalId());
            for (Gene g : B)
                familiesB.add(g.getExternalId());

            LinkedList<Integer> occA, occB;
            GeneFamily family;

            for (Map.Entry<GeneFamily, LinkedList<Integer>> entry : occMapA.entrySet()) {
                family = entry.getKey();
                occA = entry.getValue();
                occB = occMapB.get(family); // if != null, we know this family occurs in both A and B at least once
                if (occB != null && (occA.size() + occB.size()) > 2) // one to many, many to one or many to many
                    familiesWithDuplicates.addLast(family);
            }

	    this.fixed = 0;
        }
        
        /* 
         * Constructor used when the user has calculated the data by himself
         * (by some different way than the default)
         */
        public DeduplicationDCJ(
                List<Gene> A,
                List<String> familiesA,
                HashMap<GeneFamily,LinkedList<Integer>> occMapA,
                List<Gene> B,
                List<String> familiesB,
                HashMap<GeneFamily,LinkedList<Integer>> occMapB,
                LinkedList<GeneFamily> familiesWithDuplicates,
                boolean useHeuristics) {
            this.A = A;
            this.B = B;
            this.occMapA = occMapA;
            this.occMapB = occMapB;
            this.familiesWithDuplicates = familiesWithDuplicates;
            this.familiesA = familiesA;
            this.familiesB = familiesB;
            this.fixed = 0;
        }
        
        /*
         * Deduplicates genes with multiple copies in A and B, returning the
         * best possibity (the one with higher score), may use heuristics
         */
        public DCJSimilarityData computeDCJSimilarity() throws InterruptedException {

            if (parameters.useHeuristics()) {
                heuristicLongestCommonSubstrings();
                heuristicCommonAdjacencies();
                reduceGeneCopies();
            }

            // now we deduplicate recursively
            return deduplicateAndComputeDCJSimilarity();
        }

        /*
         * Heuristic to fix families by Longest Common Substrings, we assume this is
         * the first heuristic used (before other heuristics and deduplication)
         */    
        private void heuristicLongestCommonSubstrings() {

            if (familiesWithDuplicates.isEmpty()) // nothing to do
                return;

            boolean reverse = false;

            List<String> familiesAaux = getFamiliesForLCSubstr(A); // contains A plus separators
            List<String> familiesBaux = getFamiliesForLCSubstr(B);

            LCSubstrResult r = LCSubstr(familiesAaux, familiesBaux), r_orig;

            if (r.len < 3) {
                reverse = true;
                familiesBaux = ReverseLCSubstrFamilies(familiesBaux);
                r = LCSubstr(familiesAaux, familiesBaux);
            }

            while (r.len >=  3) { // try to find LCSubstrings while there are some of length 3
                r_orig = LCSubstrResultRemap(familiesAaux, familiesBaux, r, reverse);
                for (int i = 0; i < r.len; ++i) {

                    int posA = r_orig.posA + i;
                    int posB = reverse ? r_orig.posB - i : r_orig.posB + i; // if reverse, we are going from the last to the first
                    GeneFamily family = A.get(posA).getGeneFamily();               

                    // the index is shared among all subfamilies fixed by the heuristic, but differs from deduplication by the parenthesis
                    String subfamily = family.getExternalId() + "\u208D" + sub(fixed++) + "\u208E";

                    familiesA.set(posA, subfamily);
                    familiesAaux.set(r.posA+i, ""); // so next time these positions will not be considered
                    LinkedList<Integer> occA = occMapA.get(family);
                    occA.remove(new Integer(posA)); // we loose track of original occurrence positions, but this would just be used later for deduplication (and is not needed now that the gene is fixed to another one)

                    familiesB.set(posB, subfamily);
                    familiesBaux.set(r.posB+i, "");
                    LinkedList<Integer> occB = occMapB.get(family);
                    occB.remove(new Integer(posB));

                    if (occA.isEmpty() || occB.isEmpty() || (occA.size() == 1 && occB.size() == 1)) // family is now without duplicates to be assigned
                        familiesWithDuplicates.remove(family);
                }

                r = LCSubstr(familiesAaux, familiesBaux);            
                if (!reverse && r.len < 3) {
                    reverse = true;
                    familiesBaux = ReverseLCSubstrFamilies(familiesBaux);
                    r = LCSubstr(familiesAaux, familiesBaux);
                }
            }
        }
        
        /*
         * Class to easier keep track of adjacencies after we change families of genes
         */
        private class Adjacency {
            final List<Gene> G;
            final List<String> families;
            final int pos;

            public Adjacency(List<Gene> G, List<String> families, int pos) {
                this.G = G;
                this.families = families;
                this.pos = pos;
            }
            
            /* Returns a string representing the adjacency between g1 and g2 (smaller
             * family name first, if is the same family, tail first).
             */
            public String getAdjacencyStr() {
                Gene g1 = G.get(pos);
                String f1 = families.get(pos);
                Gene g2 = G.get(pos+1);
                String f2 = families.get(pos+1);
                int comp = f1.compareTo(f2);
                String adj;
                String g1str = f1 + (g1.getOrientation() == Gene.GeneOrientation.POSITIVE ? "h" : "t");
                String g2str = f2 + (g2.getOrientation() == Gene.GeneOrientation.POSITIVE ? "t" : "h");
                if (comp < 0)
                    adj = g1str + "_" + g2str;
                else if (comp > 0)
                    adj = g2str + "_" + g1str;
                else if (g1.getOrientation() == g2.getOrientation()) // same family, then tail comes first if the adjacency is a head + tail
                    adj = f1 + "t_" + f2 + "h";
                else
                    adj = g1str + "_" + g2str; // they are the same (e.g. 1t 1t)
                return adj;
            }
        
            // can't write also the hashCode method because the family may change
            // then can't use this with hashmaps, just lists and look the entire
            // list when I need to find some object
            @Override
            public boolean equals(Object obj) {
                if (this == obj)
                    return true;
		if (obj == null)
                    return false;
		if (getClass() != obj.getClass())
                    return false;
		Adjacency other = (Adjacency) obj;
		return getAdjacencyStr().equals(other.getAdjacencyStr());
            }

            @Override
            public String toString() {
                return getAdjacencyStr();
            }
        }
        
        /*
         * Heuristic that fixes adjacencies by common adjacencies
         */
        private void heuristicCommonAdjacencies() {

            List<Adjacency> adjSet = new ArrayList<>(A.size());

            /* Get adjacencies in A */
            for (int pos = 0; pos < A.size()-1; pos++) {
                GeneFamily f1 = A.get(pos).getGeneFamily();
                GeneFamily f2 = A.get(pos+1).getGeneFamily();
                String f1str = familiesA.get(pos);
                String f2str = familiesA.get(pos+1);
                if (familiesWithDuplicates.contains(f1) && f1.getExternalId().equals(f1str)     // at least one among genes in pos and pos+1...
                    || familiesWithDuplicates.contains(f2) && f2.getExternalId().equals(f2str)) // has duplicates and hasn't been fixed before
                    adjSet.add(new Adjacency(A, familiesA, pos));
            }

            /* Look now for the same adjacencies in B */
            for (int posB = 0; posB < B.size()-1; posB++) {
                GeneFamily f1 = B.get(posB).getGeneFamily();
                GeneFamily f2 = B.get(posB+1).getGeneFamily();
                String f1str = familiesB.get(posB);
                String f2str = familiesB.get(posB+1);
                if (familiesWithDuplicates.contains(f1) && f1.getExternalId().equals(f1str)       // at least one among genes in pos and pos+1...
                    || familiesWithDuplicates.contains(f2) && f2.getExternalId().equals(f2str)) { // has duplicates and hasn't been fixed before
                    Adjacency adjB = new Adjacency(B, familiesB, posB);
                    int i = adjSet.indexOf(adjB);
                    if (i != -1) {
                        Adjacency adjA = adjSet.get(i);
                        int posA = adjA.pos;
                        fixGenesCommonAdjacency(posA, posB);
                    }
                }
            }
        }

        /*
         * Reduces the number of gene copies to a reasonable amount associating
         * copies with no specific criteria
         */
        private void reduceGeneCopies() {
            LinkedList<Integer> nFamilies = new LinkedList<>();
            int n;
            
            // number of ways of associating genes in each family
            for (GeneFamily family : familiesWithDuplicates) {
                int noccA = occMapA.get(family).size();
                int noccB = occMapB.get(family).size();
                nFamilies.add(noccA > noccB ? nPr(noccA, noccB) : nPr(noccB, noccA));
            }
            
            // number of ways to associate genes taking into account all families
            n = multiplyList(nFamilies);
            
            while (n > MAX_COMBINATIONS) {
                int nFamilyOld = nFamilies.getFirst();
                GeneFamily family = familiesWithDuplicates.getFirst();               
                String subfamily = family.getExternalId() + "\u208D" + sub(fixed++) + "\u208E";
                
                LinkedList<Integer> occA = occMapA.get(family);
                LinkedList<Integer> occB = occMapB.get(family);
                int noccA = occA.size();
                int noccB = occB.size();
                
                int posA = occA.pop(); // position of gene to be deduplicated
                familiesA.set(posA, subfamily);
                
                int posB = occB.pop();                
                familiesB.set(posB, subfamily);
                
                int nFamilyNew;
                if (nFamilyOld == Integer.MAX_VALUE)
                    nFamilyNew = noccA > noccB ? nPr(noccA-1, noccB-1) : nPr(noccB-1, noccA-1); // have to try to calculate again
                else
                    nFamilyNew = nFamilyOld / (noccA > noccB ? noccA : noccB); // nPr(n,r) / n = nPr(n-1, r-1)
                
                if (occA.isEmpty() || occB.isEmpty() || (occA.size() == 1 && occB.size() == 1)) { // family is now without duplicates to be assigned
                    familiesWithDuplicates.removeFirst();
                    nFamilies.removeFirst();
                }
                else
                  nFamilies.set(0, nFamilyNew);
                
                if (n == Integer.MAX_VALUE)
                    n = multiplyList(nFamilies);
                else
                    n = (n / nFamilyOld) * nFamilyNew;
            }
        }

        /*
         * Recursive step (for each family with duplicates) computing for a family
         * all possible associations by the non-recursive Heap's permutation algorithm
         * for all combinations (permutations of all combinations of k elements among n)
         */
        private DCJSimilarityData deduplicateAndComputeDCJSimilarity() throws InterruptedException {

            if (task != null && task.isCancelled()) throw new InterruptedException();

            if (familiesWithDuplicates.isEmpty())
                return DCJSimilarity.this.computeDCJSimilarity(A, familiesA, B, familiesB);

            DCJSimilarityData best = null;
            GeneFamily family = familiesWithDuplicates.removeFirst();
            
            LinkedList<Integer> occA = occMapA.get(family);
            LinkedList<Integer> occB = occMapB.get(family);

            boolean stop = false;
            int n = occB.size();
            int k = occA.size();
            boolean swapped = false; // if we swapped n and k because n was smaller than k
            if (n < k) {
                n = occA.size();
                k = occB.size();
                swapped = true;
            }
            
            int[] combination = new int[k];
            for (int i = 0; i < k; i++)
                combination[i] = i;
        
            int[] permutation = new int[k];
            int[] rev_permutation = new int[n];
            
            while (!stop) {
                                        
                int i = 0, j;
                int[] c = new int[k]; // initialized with zeros by default
                int aux;

                for (j = 0; j < k; j++)
                    permutation[j] = combination[j];

                if (swapped) {
                    reversePermutation(permutation, rev_permutation);
                    fixFamily(family.getExternalId(), rev_permutation, occA, occB);
                }
                else
                    fixFamily(family.getExternalId(), permutation, occA, occB);
                
                best = deduplicateAndComputeDCJSimilarity();

                // generates all permutations for the current combination
                while (i < k) {
                    if (c[i] < i) {
                        if (i % 2 == 0) {
                            aux = permutation[0]; permutation[0] = permutation[i]; permutation[i] = aux; } //swap(permutation[0], permutation[i]);
                        else {
                            aux = permutation[c[i]]; permutation[c[i]] = permutation[i]; permutation[i] = aux; } //swap(permutation[c[i]], permutation[i]);

                        if (swapped) {
                            reversePermutation(permutation, rev_permutation);
                            fixFamily(family.getExternalId(), rev_permutation, occA, occB);
                        }
                        else
                            fixFamily(family.getExternalId(), permutation, occA, occB);
                        best = bestDCJSimilarityResult(best, deduplicateAndComputeDCJSimilarity());

                        c[i]++;
                        i = 0;
                    }
                    else {
                        c[i] = 0;
                        i++;
                    }
                }
                
                if (!nextCombination(combination, n, k))
                    stop = true;
            }

            familiesWithDuplicates.addFirst(family);
            // because we want to revert family arrays as they were before
            revertFamily(family.getExternalId(), occA, occB);
            return best;
        }


        

        /*
         * Returns a string representing some int as subscript
         */
        private String sub(int n) {
            String str = Integer.toString(n);
            StringBuilder sub = new StringBuilder(n < 0 ? "\u208B" : "");
            for (int i = n < 0 ? 1 : 0; i < str.length(); i++)
                sub.append(Character.toChars(0x2080 + str.charAt(i) - '0'));
            return sub.toString();
        }





        /*
          Obtains the next combinations of k elements between 0 and n-1, returns
          true if we got the next combination, and false if we have gereated all,
          based on: http://www.martinbroadhurst.com/combinations.html
        */
        boolean nextCombination(int[] combination, int n, int k)
        {
            boolean finished = false;
            boolean changed = false;
            int i;

            if (k > 0) {
                for (i = k - 1; !finished && !changed; i--) {
                    if (combination[i] < (n - 1) - (k - 1) + i) {
                        // Increment this element
                        combination[i]++;
                        if (i < k - 1) {
                            // Turn the elements after it into a linear sequence
                            int j;
                            for (j = i + 1; j < k; j++) {
                                combination[j] = combination[j - 1] + 1;
                            }
                        }
                        changed = true;
                    }
                    finished = i == 0;
                }
                if (!changed) {
                    // Reset to first combination
                    for (i = 0; i < k; i++) {
                        combination[i] = i;
                    }
                }
            }
            return changed;
        }

        /*
         * Gets the permutations and maps its values to positions in rev_permutation
         */
        private void reversePermutation(int[] permutation, int[] rev_permutation) {
            for (int i = 0; i < rev_permutation.length; i++)
                rev_permutation[i] = -1;
            for (int i = 0; i < permutation.length; i++)
                rev_permutation[permutation[i]] = i;
        }
                
        /*
         * Stores into family (string) arrays the assignment defined by permutation
         * for some family, defining "new" families named by the original family
         * name followed by a subscript number (we assume no original family name
         * has subscript numbers)
         */
        private void fixFamily(String family, int[] permutation, LinkedList<Integer> occA, LinkedList<Integer> occB) {
            int count = 0;
            for (int i = 0; i < permutation.length; i++) // if A has moree elements than B, we have -1 to indicate there is no gene associaton
                if (i < occA.size())
                    if (permutation[i] != -1 && permutation[i] < occB.size()) {
                        String subfamily = family + sub(count++);
                        familiesA.set(occA.get(i), subfamily);
                        familiesB.set(occB.get(permutation[i]), subfamily);
                    }
                    else
                        familiesA.set(occA.get(i), null);
                else
                    familiesB.set(occB.get(permutation[i]), null);
            if (occB.size() > occA.size()) { // if B is greater than A, we have to find wich genes will not be associated to any of A
                ArrayList<Integer> p = new ArrayList<>(permutation.length);
                for (int i = 0; i < permutation.length; i++)
                    p.add(permutation[i]);
                for (int i = 0; i < occB.size(); i++)
                    if (!p.contains(i))
                        familiesB.set(occB.get(i), null);
            }
        }

        /*
         * Reverts the family array for one family after being modified by fixFamily
         */
        private void revertFamily(String family, LinkedList<Integer> occA, LinkedList<Integer> occB) {
            for (int i : occA)
                familiesA.set(i, family);
            for (int i : occB)
                familiesB.set(i, family);
        }

        /*
         * Returns a hashmap containing for each family the positions it occurs
         */
        private HashMap<GeneFamily,LinkedList<Integer>> familyOccurrencesMap(List<Gene> genome) {
            HashMap<GeneFamily,LinkedList<Integer>> map = new HashMap<>( (int) (2 * genome.size()) );
            int i;

            i = 0;
            for (Gene g : genome) {
                LinkedList<Integer> occ = map.get(g.getGeneFamily());
                if (occ == null) {
                    map.put(g.getGeneFamily(), new LinkedList<Integer>());
                    occ = map.get(g.getGeneFamily());
                }
                occ.add(i++);
            }
            return map;
        }





        /*
         * Prints a list of genes given the list of genes and the strings of families
         */
        private void printGenesFamilies(List<Gene> G, List<String> F) {
            for (int i = 0; i < G.size(); ++i)
                System.out.print((G.get(i).getOrientation() == Gene.GeneOrientation.POSITIVE ? '+' : '-') + F.get(i) + " ");
            System.out.println("");
        }

        /*
         * Fixes genes involved in 2 common adjacencies and returns the new total
         * nuber of fixed genes pairs (+1 or +2), updates familiesWithDuplicates and
         * occMaps if necessary
         */
        private void fixGenesCommonAdjacency(
                int posA,
                int posB) {

            boolean reversed = !familiesA.get(posA).equals(familiesB.get(posB)) || A.get(posA).getOrientation() != B.get(posB).getOrientation(); // the first gene in A corresponds to the second gene in B (in the sense of their order in genome)
            posB = reversed ? posB+1 : posB;

            for (int i = 0; i < 2; i++) { // just to don't have to duplicate this code
                GeneFamily family = A.get(posA).getGeneFamily();
                if (family.getExternalId().equals(familiesA.get(posA))) { // then this genes has not been fixed before (the string in familiesA is the original one)
                    String subfamily = family.getExternalId() + "\u208D" + sub(fixed++) + "\u208E";
                    familiesA.set(posA, subfamily);
                    familiesB.set(posB, subfamily);
                    List<Integer> occA = occMapA.get(family);
                    occA.remove(new Integer(posA)); // otherwise the wrong remove method will be called
                    List<Integer> occB = occMapB.get(family);
                    occB.remove(new Integer(posB));
                    if (occA.isEmpty() || occB.isEmpty() || (occA.size() == 1 && occB.size() == 1)) // family is now without duplicates to be assigned
                        familiesWithDuplicates.remove(family);
                }
                // update positions to second (and last) pair
                posA++;
                posB = reversed ? posB-1 : posB+1;
            }
        }





        /*
         * Class just used do return results of the Longest Common Substring
         */
        private class LCSubstrResult {
            int posA;
            int posB;
            int len;
            LCSubstrResult(int posA, int posB, int len) { this.posA = posA; this.posB = posB; this.len = len; }
        }

        /*
         * Returns a list of families to be used with LCSubstr, replacing by "" 
         * genes that are not preceded or followed by some other gene with duplicates
         * add placing a separator null between contiguous substrings with duplicate genes,
         * the returned sequence may be bigger than the original one
         */
        private List<String> getFamiliesForLCSubstr(List<Gene> G) {
            int pos = 0;
            List<String> families = new ArrayList<>((int) (1.5 * G.size()));

            for (Gene g : G) {
                if (familiesWithDuplicates.contains(g.getGeneFamily()))
                    families.add( (g.getOrientation() == Gene.GeneOrientation.POSITIVE ? "+" : "-") + g.getExternalId() );
                else // not duplicate
                    if (pos > 0 && familiesWithDuplicates.contains(G.get(pos-1).getGeneFamily())) { // preceded  by some other gene with duplicates
                        families.add( (g.getOrientation() == Gene.GeneOrientation.POSITIVE ? "+" : "-") + g.getExternalId() );
                        if(pos < G.size()-1 && !familiesWithDuplicates.contains(G.get(pos+1).getGeneFamily())) // not followed by some other gene with duplicates
                            families.add(null);
                    }
                    else if(pos < G.size()-1 && familiesWithDuplicates.contains(G.get(pos+1).getGeneFamily())) // followed by some other gene with duplicates
                        families.add( (g.getOrientation() == Gene.GeneOrientation.POSITIVE ? "+" : "-") + g.getExternalId() );
                    else
                        families.add("");
                ++pos;
            }

            return families;
        }

        /*
         * Longest Common Substring based on:
         *   https://www.geeksforgeeks.org/print-longest-common-substring/
         */
        private LCSubstrResult LCSubstr(List<String> X, List<String> Y) {
            int m = X.size();
            int n = Y.size();
            // Create a table to store lengths of longest common 
            // suffixes of substrings.   Note that LCSuff[i][j] 
            // contains length of longest common suffix of X[0..i-1] 
            // and Y[0..j-1]. The first row and first column entries 
            // have no logical meaning, they are used only for 
            // simplicity of program 
            int[][] LCSuff = new int[m + 1][n + 1]; 

            // To store length of the longest common substring 
            int len = 0; 

            // To store the index of the cell which contains the 
            // maximum value. This cell's index helps in building 
            // up the longest common substring from right to left. 
            int row = 0, col = 0; 

            /* Following steps build LCSuff[m+1][n+1] in bottom 
               up fashion. */
            for (int i = 0; i <= m; i++) { 
                for (int j = 0; j <= n; j++) { 
                    if (i == 0 || j == 0) 
                        LCSuff[i][j] = 0; 
                    else if (X.get(i - 1) != null &&
                             !X.get(i - 1).isEmpty() &&
                             Y.get(j - 1) != null &&
                             !Y.get(j - 1).isEmpty() &&
                             X.get(i - 1).equals(Y.get(j - 1))) { 
                        LCSuff[i][j] = LCSuff[i - 1][j - 1] + 1; 
                        if (len < LCSuff[i][j]) { 
                            len = LCSuff[i][j]; 
                            row = i; 
                            col = j; 
                        } 
                    } 
                    else
                        LCSuff[i][j] = 0; 
                } 
            } 

            //System.out.print("LCSubstr (" + (row-len) + "," + (col-len) + "," + len + "): ");
            LCSubstrResult r = new LCSubstrResult(row-len,col-len,len);

            // if true, then no common substring exists 
            if (len == 0)
                return new LCSubstrResult(0,0,0);         


            // allocate space for the longest common substring
            LinkedList<String> LCSubstr = new LinkedList<>();

            // traverse up diagonally form the (row, col) cell 
            // until LCSuff[row][col] != 0 
            while (LCSuff[row][col] != 0) {
                LCSubstr.addFirst(X.get(row - 1)); // or Y[col-1] 
                --len; 

                // move diagonally up to previous cell 
                row--; 
                col--; 
            } 

            //System.out.println(LCSubstr);
            return r;
        }

        /*
         * Remaps positions discarding separators (null), reverse = true means
         * B is reversed and we want to map positions to the original one
         * (not reversed) and in this case the position will be the LAST one of
         * the substring match in B)
         */
        private LCSubstrResult LCSubstrResultRemap(
                List<String> familiesAWithSep,
                List<String> familiesBWithSep,
                LCSubstrResult r, boolean reverse) {
            int posA, posB;
            int sepcount = 0;
            for (int i = 0; i < r.posA; ++i)
                if (familiesAWithSep.get(i) == null)
                    ++sepcount;
            posA = r.posA - sepcount;
            sepcount = 0;
            if (!reverse) {
                for (int i = 0; i < r.posB; ++i)
                    if (familiesBWithSep.get(i) == null)
                        ++sepcount;
                posB = r.posB - sepcount;
            }
            else {
                for (int i = r.posB+1; i < familiesBWithSep.size(); ++i)
                    if (familiesBWithSep.get(i) == null)
                        ++sepcount;
                posB = familiesBWithSep.size() - 1 - r.posB - sepcount;
            }

            return new LCSubstrResult(posA, posB, r.len);
        }

        /*
         * Reverses a sequence of families (as strings)
         */
        private List<String> ReverseLCSubstrFamilies(List<String> families) {
            List<String> rev = new ArrayList<>(families.size());
            for (int i = families.size()-1; i >= 0; --i) {
                String s = families.get(i);
                if (s == null || s.isEmpty())
                    rev.add(s);
                else
                    rev.add( (s.charAt(0) == '+' ? '-' : '+') + s.substring(1, s.length()));
            }
            return rev;
        }
        
        /*
         * Returns nPr or Integer.MAX_VALUE if nPr is greater than we can store in a long
         */
        public int nPr(int n, int r) {
            if (n <= 0 || r <= 0)
                throw new ArithmeticException("n and r values must be greater than 0!");
            if (n < r)
                throw new ArithmeticException("n must be greater than r!");
            int p = 1;
            try {
                for (int factor = n-r+1; factor <= n; ++factor)
                    p = Math.multiplyExact(p, factor);
            }
            catch (ArithmeticException e) {
                return Integer.MAX_VALUE;
            }
            return p;
        }
        
        /*
         * Multiplies elements of list, returns Integer.MAX_VALUE if the result
         * is greater than that
         */
        private int multiplyList(List<Integer> l) {
            return multiplyList(l, 0);
        }
        private int multiplyList(List<Integer> l, int start) {
            int total = 1;
            
            ListIterator<Integer> it = l.listIterator(start);
            
            try {
                while (it.hasNext())
                    total = Math.multiplyExact(total, it.next());
            }
            catch (ArithmeticException e) {
                return Integer.MAX_VALUE;
            }
            return total;
        }
    }
}
