/*
 * Copyright 2019 Diego Rubert
 *
 * This file is part of Gecko3.
 *
 * Gecko3 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gecko3 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Gecko3.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Thread for computing the DCJSimilarity for all refseqs/seqs of one cluster
 * @author Diego Rubert
 */

package de.unijena.bioinf.gecko3.util;

import de.unijena.bioinf.gecko3.algo.DCJSimilarity;
import de.unijena.bioinf.gecko3.datastructures.Chromosome;
import de.unijena.bioinf.gecko3.datastructures.GeneCluster;
import de.unijena.bioinf.gecko3.datastructures.Genome;
import de.unijena.bioinf.gecko3.datastructures.Parameter;
import de.unijena.bioinf.gecko3.datastructures.Subsequence;
import de.unijena.bioinf.gecko3.datastructures.util.DCJSimilarityData;
import de.unijena.bioinf.gecko3.gui.DCJProgressBarWindowAllClusters;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import javax.swing.SwingWorker;


public class DCJSimilarityTaskCluster extends SwingWorker<Void, String> {

    private final GeneCluster cluster;
    private final int refSeqIndex; // the index of the genome which is the reference for this cluster
    private final Subsequence[][] subsequences;
    private final List<List<String>> chromosomes;
    private final List<List<Chromosome>> chromosomesAsObjects;
    private final Genome[] genomes;
    private final Parameter parameters;
    private final DCJProgressBarWindowAllClusters progressBar;
    private final boolean discardAdjacencyGraphs;
    
    private int numberOfSubtasks;
    private int progress; // the number of subtasks computed
    
    private final boolean usingSequenceMatrix; // When the task is running for results export, we have the subsequences stored in a different way (and sometimes not all of them)

    public DCJSimilarityTaskCluster(GeneCluster cluster, Subsequence[][] subsequences, List<List<String>> chromosomes, List<List<Chromosome>> chromosomesAsObjects, Parameter parameters, DCJProgressBarWindowAllClusters progressBar, boolean discardAdjacencyGraphs) {
        super();
        
        this.cluster = cluster;
        this.refSeqIndex = cluster.getRefSeqIndex();
        this.subsequences = subsequences;
        this.chromosomes = chromosomes;
        this.chromosomesAsObjects = chromosomesAsObjects;
        this.genomes = null;
        this.parameters = parameters;
        this.progressBar = progressBar;
        this.discardAdjacencyGraphs = discardAdjacencyGraphs;
        
        this.usingSequenceMatrix = true;

        this.numberOfSubtasks = 0;
        for (int ref = 0; ref < chromosomes.get(refSeqIndex).size(); ref++)
            for (int i = 0; i < chromosomes.size(); i++) {
                if (chromosomes.get(i) == null)
                    continue;
                for (int j = 0; j < chromosomes.get(i).size(); j++) {
                    if (i == refSeqIndex && j != ref)
                        continue;            
                    this.numberOfSubtasks++;
                }
            }
    }
    
    public DCJSimilarityTaskCluster(GeneCluster cluster, Genome[] genomes, Parameter parameters, DCJProgressBarWindowAllClusters progressBar, boolean discardAdjacencyGraphs) {
        super();
        this.cluster = cluster;
        this.refSeqIndex = cluster.getRefSeqIndex();
        this.subsequences = cluster.getOccurrences(true).getSubsequences();
        this.chromosomes = null;
        this.chromosomesAsObjects = null;
        this.genomes = genomes;
        this.parameters = parameters;
        this.progressBar = progressBar;
        this.discardAdjacencyGraphs = discardAdjacencyGraphs;

        this.usingSequenceMatrix = false;

        this.numberOfSubtasks = 0;
        for (int ref = 0; ref < subsequences[refSeqIndex].length; ref++) 
            for (int i = 0; i < subsequences.length; i++) {
                if (genomes[i].getChromosomes().isEmpty())
                    continue;
                for (int j = 0; j < subsequences[i].length; j++) {
                    if (i == refSeqIndex && j != ref)
                        continue;
                    numberOfSubtasks++;
                }
            }
    }
    
    /*
     * Returns the internal counter of subtasks computed
     */
    public int getComputedSubtasks() {
        return this.progress;
    }
    
    /*
     * Computes and returns the number of DCJ similarities to be computed
     * (including the ones computed previously by the graphic interface)
     */
    public int getNumberOfSubtasks() {
        return this.numberOfSubtasks;
    }
    
    /*
     * Returns the cluster
     */
    public GeneCluster getCluster() {
        return this.cluster;
    }
    
    /*
     * Calls the local DCJ similarity computation for each sequence and updates progress
     */
    @Override
    public Void doInBackground() throws IOException {
        if (usingSequenceMatrix)
            return computeAllUsingSequenceMatrix();
        else
            return computeAll();
    }

    /*
     * Receives data chunks (chromosome names) from the publish method
     * asynchronously on the Event Dispatch Thread
     */
    @Override
    protected void process(final List<String> chunks) {
        if (progressBar != null)
            for (String s : chunks)
                progressBar.appendResultText(s);
    }
    
    /*
     * Executed in event dispatching thread
     */
    @Override
    public void done() {
        try {
            get(); // we do get() only to get an stacktrace if an exception occurred
        } catch (InterruptedException | CancellationException ex) {
            // no problem, the user can cancel the task
            // System.out.println("DCJ computation canceled (3)!!!!");
        } catch (ExecutionException ex) {
            System.out.println("Exception: " + ex);
            throw new RuntimeException(ex.getCause());
        }
        super.done();
    }
    
    /*
     * Computes the results when the sequence matrix is given (usually for
     * result exporting, sometimes with just a subset of all subsequences found)
     */
    private Void computeAllUsingSequenceMatrix() throws IOException {
        DCJSimilarity dcj = new DCJSimilarity(parameters, this);
        DCJSimilarityData dcjSimData;
                
        progress = 0;
        setProgress(0);
                
        try {
            for (int ref = 0; ref < chromosomes.get(refSeqIndex).size(); ref++) {
                Subsequence refSeq = subsequences[refSeqIndex][ref];
                
                String refChrStr = chromosomes.get(refSeqIndex).size() <= 1 ? String.valueOf(refSeqIndex + 1) : String.format("%d.%d", refSeqIndex + 1, ref);
                publish("  Refseq " +  refChrStr);
                
                for (int i = 0; i < chromosomes.size(); i++) {
                    if (chromosomes.get(i) == null)
                        continue;
                    
                    for (int j = 0; j < chromosomes.get(i).size(); j++) { // chromosomes.get(i) returns the list of chromosomes in genome i
                        if (i == refSeqIndex && j != ref)
                            continue; // we want the similarity from the refseq to itself (the best possible) but not to another refseqs
                        
                        String progressStr = chromosomes.get(i).size() <= 1 ? String.valueOf(i + 1) : String.format("%d.%d", i + 1, j);
                        
                        Subsequence seq = subsequences[i][j]; // genome i, subsequence j in the genome i
                        
                        dcjSimData = seq.getDCJSim(refSeq);
                        
                        //if (dcjSimData == null) Thread.sleep(1000);
                        if (dcjSimData == null) {
                            dcjSimData = dcj.computeDCJSimilarity(subsequences[refSeqIndex][ref],
                                                                  chromosomesAsObjects.get(refSeqIndex).get(ref),
                                                                  seq,
                                                                  chromosomesAsObjects.get(i).get(j)); // compute
                            if (dcjSimData == null)
                                dcjSimData = DCJSimilarityData.noSimilarity(refSeq, seq);
                            seq.addDCJSim(refSeq, dcjSimData);
                        }
                        
                        if (dcjSimData != null && !dcjSimData.noSimilarity()) {
                            /*
                            // New label: Adjacency Graph Cluster ID cluster_Id refseq_nr[interval] seq_nr[interval]
                            if (dcjSimData.ag() != null)
                                dcjSimData.ag().setLabel(String.format("Cluster ID %d %s[%d-%d] %s[%d-%d]",
                                        cluster.getId(),
                                        refChrStr,
                                        dcjSimData.startA(),
                                        dcjSimData.stopA(),
                                        chrStr,
                                        dcjSimData.startB(),
                                        dcjSimData.stopB()));*/
                            // We will not use the adjacency graph (at least in the current version), then we don't need to waste memory
                            if (dcjSimData.ag() != null && discardAdjacencyGraphs)
                                dcjSimData.discardAg();
                        }

                        progress++;
                        setProgress((int) (100 * progress / (float) numberOfSubtasks));
                        publish("    " + progressStr + " " + chromosomes.get(i).get(j));
                        
                    }
                }
            }
        } catch (InterruptedException e) {
            // System.out.println("DCJ computation canceled (1)!!!");
        }
        if (this.isCancelled()) {
            // System.out.println("DCJ computation canceled (2)!!!!");
            return null;
        }

        //try { Thread.sleep(200); } catch (InterruptedException ignore) { }
        return null;
    }
    
    /*
     * Computes the local DCJ similarity for all subsequences
     */
    public Void computeAll() throws IOException {
        DCJSimilarity dcj = new DCJSimilarity(parameters, this);
        DCJSimilarityData dcjSimData;
                
        progress = 0;
        setProgress(0);
        
        //System.out.println("subsequences len: " + subsequences.length + ", genomes len: " + genomes.length + ", index: " + refSeqIndex);
        
        try {
            for (int ref = 0; ref < subsequences[refSeqIndex].length; ref++) {
                //System.out.println("Refseq: " + subsequences[refSeqIndex][ref]);
                Subsequence refSeq = subsequences[refSeqIndex][ref];
                Chromosome refSeqChr = genomes[refSeqIndex].getChromosomes().get(refSeq.getChromosome());
                
                for (int i = 0; i < subsequences.length; i++) {
                    if (genomes[i].getChromosomes().isEmpty())
                        continue;
                    //System.out.println("\tGenome " + i + " (" + genomes[i].getName() + ")");
                    
                    for (int j = 0; j < subsequences[i].length; j++) {
                        if (i == refSeqIndex && j != ref)
                            continue; // we want the similarity from the refseq to itself (the best possible) but not to another refseqs
                        
                        Subsequence seq = subsequences[i][j];
                        Chromosome chr = genomes[i].getChromosomes().get(seq.getChromosome());
                        //System.out.println("\t\tSeq: " + seq + ", in chr:" + chr);

                        dcjSimData = seq.getDCJSim(refSeq);
                        
                        //if (dcjSimData == null) Thread.sleep(1000);
                        if (dcjSimData == null) {
                            dcjSimData = dcj.computeDCJSimilarity(refSeq,
                                                                  refSeqChr,
                                                                  seq,
                                                                  chr); // compute
                            if (dcjSimData == null)
                                dcjSimData = DCJSimilarityData.noSimilarity(refSeq, seq);
                            seq.addDCJSim(refSeq, dcjSimData);
                        }
                        
                        if (dcjSimData != null && !dcjSimData.noSimilarity()) {
                            // We will not use the adjacency graph (at least in the current version), then we don't need to waste memory
                            if (dcjSimData.ag() != null && discardAdjacencyGraphs)
                                dcjSimData.discardAg();
                        }
                        
                        String progressStr = String.format("Ref. seq.: %d, Genome: %s, Subseq.: %d", ref, genomes[i].getName(), j);

                        progress++;
                        setProgress((int) (100 * progress / (float) numberOfSubtasks));
                        publish("    " + progressStr);
                        
                    }
                }
            }
        } catch (InterruptedException e) {
            // System.out.println("DCJ computation canceled (1)!!!");
        }
        if (this.isCancelled()) {
            // System.out.println("DCJ computation canceled (2)!!!!");
            return null;
        }

        //try { Thread.sleep(200); } catch (InterruptedException ignore) { }
        return null;
    }
}
