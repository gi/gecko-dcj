/*
 * Copyright 2019 Diego Rubert
 *
 * This file is part of Gecko3.
 *
 * Gecko3 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gecko3 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Gecko3.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Progress bar showed during the calculation of the local DCJ similarity of clusters
 * @author Diego Rubert
 */


package de.unijena.bioinf.gecko3.gui;

import de.unijena.bioinf.gecko3.datastructures.Chromosome;
import de.unijena.bioinf.gecko3.gui.util.DCJSimilarityTask;
import de.unijena.bioinf.gecko3.gui.util.DisabledGlassPane;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.beans.*;
import java.util.List;
import javax.swing.text.DefaultCaret;


public class DCJProgressBarWindow extends JFrame {

    private final DCJProgressBar progressBar;
    
    public DCJProgressBarWindow(List<Chromosome> chromosomes, DCJSimilarityTask task, int numberOfComputations, JRootPane rootPane) {
        super("Computing local DCJ similarity...");

        this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        progressBar = new DCJProgressBar(chromosomes, task, numberOfComputations, this, rootPane); // we need the root pane to disable it
        progressBar.setOpaque(true); //content panes must be opaque
        this.setContentPane(progressBar);
        this.setResizable(false);
        this.setIconImage(new ImageIcon(ClassLoader.getSystemResource("images/computing.png")).getImage());
        this.pack();
    }
    
    /*
     * Waits the DCJ computation task to finish
     */
    public void waitDCJComputationTask() {
        progressBar.waitDCJComputationTask();
    }     

    private class DCJProgressBar extends JPanel
            implements ActionListener,
            PropertyChangeListener {

        private final JProgressBar progressBar;
        private final JButton cancelButton;
        private final JTextArea taskOutput;
        private final DCJSimilarityTask task;
        private final DisabledGlassPane glassPane;
        private final JFrame progressBarWindow;
        private final List<Chromosome> chromosomes;
        private int lastProgress;

        public DCJProgressBar(List<Chromosome> chromosomes, DCJSimilarityTask task, int numberOfComputations, JFrame progressBarWindow, JRootPane rootPane) {
            super(new BorderLayout());

            this.glassPane = new DisabledGlassPane();
            rootPane.setGlassPane(glassPane); // set the glasspane for the rootpane (of the main application)
            this.progressBarWindow = progressBarWindow; // save the window (JFrame) so we can close it later
            this.chromosomes = chromosomes;
            this.task = task;
            this.lastProgress = 0;

            cancelButton = new JButton("Cancel");
            cancelButton.setActionCommand("cancel");
            cancelButton.addActionListener(this);

            progressBar = new JProgressBar(0, numberOfComputations);
            progressBar.setValue(0);
            progressBar.setStringPainted(true);
            progressBar.setBorder(BorderFactory.createEmptyBorder(0,0,0,0));

            taskOutput = new JTextArea(8, 40);
            taskOutput.setMargin(new Insets(5, 5, 5, 5));
            taskOutput.setEditable(false);
            JScrollPane taskOutputScroll = new JScrollPane(taskOutput);
            DefaultCaret caret = (DefaultCaret)taskOutput.getCaret();
            caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);

            this.setLayout(new BorderLayout(5,5));
            
            add(progressBar, BorderLayout.PAGE_START);
            add(taskOutputScroll, BorderLayout.CENTER);
            add(cancelButton, BorderLayout.PAGE_END);
            setBorder(BorderFactory.createEmptyBorder(10, 15, 10, 15));
            
            begin();
        }

        /*
         * Cancel button pressed
         */
        @Override
        public void actionPerformed(ActionEvent evt) {
            task.cancel(true);
        }

        /*
         * Progress property changed
         */
        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            if ("progress".equals(evt.getPropertyName())) {
                int progress = task.getComputedSubtasks();
                //(Integer) evt.getNewValue(); // this would return an percentage, not the count of subtasks done
                // task.getProgress() // this would return the percentage too
                progressBar.setValue(progress);
                while (lastProgress < progress) // we can't assume this function will be called for each progress increase
                    taskOutput.append(chromosomes.get(lastProgress++).getFullName() + "\n");
            }
        }
        
        /*
         * Prepares the window to show computation
         */
        private void begin() {
            glassPane.activate();
            setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            task.addPropertyChangeListener(this);
            task.execute();
        }
        
        /*
         * Returns things as they were before starting computation
         */
        private void end() {
            setCursor(null); //turn off the wait cursor
            glassPane.deactivate();
            progressBarWindow.setVisible(false);
            progressBarWindow.dispose();
        }
        
        /*
         * Waits the DCJ computation task to finish
         */
        public void waitDCJComputationTask() {
            while(!task.isDone()) {
                Thread.yield();
            }
            end();
        }                
        
    }

}