/*
 * Copyright 2019 Diego Rubert
 * Copyright 2014 Sascha Winter, Tobias Mann, Hans-Martin Haase, Leon Kuchenbecker and Katharina Jahn
 *
 * This file is part of Gecko3.
 *
 * Gecko3 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gecko3 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Gecko3.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.unijena.bioinf.gecko3.gui;

import ca.odell.glazedlists.BasicEventList;
import ca.odell.glazedlists.EventList;
import ca.odell.glazedlists.swing.AutoCompleteSupport;
import com.jgoodies.forms.builder.ButtonBarBuilder;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import de.unijena.bioinf.gecko3.GeckoInstance;
import de.unijena.bioinf.gecko3.datastructures.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.PlainDocument;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.prefs.Preferences;

public class StartComputationDialog extends JDialog {

    private static final Logger logger = LoggerFactory.getLogger(StartComputationDialog.class);
    private static final long serialVersionUID = -5635614016950101153L;

    private int quorum;
    private final JComboBox<String> dstCombo;
    private final JComboBox<String> qCombo;
    private Parameter.OperationMode opMode;
    private Parameter.ReferenceType refType;
    private final JComboBox<Parameter.ReferenceType> refCombo;
    private final JPanel additionalRefClusterSettings;
    
    final JTabbedPane tabbedDistancePane;

    private final JCheckBox mergeResults;
    private final JCheckBox refInRef;

    private final DeltaTable deltaTable;
    private final JSpinner distanceSpinner;
    private final JSpinner sizeSpinner;
    
    private final JFormattedTextField dcjDiscardedPenality;
    private final JSpinner dcjBorderlineCycleLength;
    private final JCheckBox dcjComputeSubintervals;
    private final JSpinner dcjExtendSubsequences;
    private final JCheckBox dcjPreCompute;
    private final JCheckBox dcjUseHeuristics;
    private static final int DCJ_MAX_EXTENDED_SUBINTERVALS_SIZE = 5;

    
    public StartComputationDialog(JFrame parent) {
        final GeckoInstance gecko = GeckoInstance.getInstance();
        this.setModalityType(DEFAULT_MODALITY_TYPE);
        this.setResizable(false);
        setIconImages(parent.getIconImages());
        this.setTitle("Configure computation");

        this.opMode = Parameter.OperationMode.reference;
        this.refType = Parameter.ReferenceType.allAgainstAll;

        this.sizeSpinner = new JSpinner(new SpinnerNumberModel(7, 0, Integer.MAX_VALUE, 1));
        this.distanceSpinner = new JSpinner(new SpinnerNumberModel(3, 0, Integer.MAX_VALUE, 1));
        this.deltaTable = new DeltaTable();

        NumberFormat format = DecimalFormat.getInstance();
        format.setGroupingUsed(false);
        format.setMinimumFractionDigits(1);
        format.setMaximumFractionDigits(4);
        format.setRoundingMode(RoundingMode.HALF_UP);
        this.dcjDiscardedPenality = new JFormattedTextField(format);
        this.dcjDiscardedPenality.setHorizontalAlignment(JTextField.RIGHT);
        this.dcjDiscardedPenality.setToolTipText("Penality can't be negative.");
        this.dcjDiscardedPenality.setValue(Parameter.DEFAULT_DISCARDED_PENALITY);
        this.dcjDiscardedPenality.setColumns(6);
        this.dcjDiscardedPenality.addPropertyChangeListener("value", new PropertyChangeListener() {
            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                if ( ((Number)evt.getNewValue()).doubleValue() < 0)
                    dcjDiscardedPenality.setValue(0.0);
            }
        });

        this.dcjBorderlineCycleLength = new JSpinner(new SpinnerNumberModel(Parameter.DEFAULT_BORDERLINE_LENGTH, 2, Integer.MAX_VALUE, 2));
        this.dcjBorderlineCycleLength.setPreferredSize(new Dimension(50, this.dcjBorderlineCycleLength.getPreferredSize().height));
        this.dcjBorderlineCycleLength.setToolTipText("<html>In the adjacency graph, cycles shorter than this<br>get positive score and longer get negative score.</html>");
        
        this.dcjExtendSubsequences = new JSpinner(new SpinnerNumberModel(Parameter.DEFAULT_EXTEND_SUBSEQUENCES, 0, DCJ_MAX_EXTENDED_SUBINTERVALS_SIZE, 1));
        this.dcjExtendSubsequences.setPreferredSize(new Dimension(50, this.dcjExtendSubsequences.getPreferredSize().height));
        this.dcjExtendSubsequences.setToolTipText("<html>The number of extra genes considered before<br>the beginning and after the end of subsequences.</html>");
        this.dcjExtendSubsequences.setEnabled(false);
                
        this.dcjComputeSubintervals = new JCheckBox("", Parameter.DEFAULT_COMPUTE_SUBINTERVALS);
        this.dcjComputeSubintervals.setToolTipText("<html>If checked, the local DCJ similarity will be computed for every subinterval<br>of subsequences and only the best will be shown for each subsequence.<br><b>*This can be highly time consuming for very large clusters!*</b></html>");
        this.dcjComputeSubintervals.setBackground(new Color(0,0,0,0));
        this.dcjComputeSubintervals.addItemListener(new ItemListener(){
                @Override
                public void itemStateChanged(ItemEvent e) {
                    if(e.getStateChange() == ItemEvent.SELECTED){
                        dcjExtendSubsequences.setEnabled(true);
                    }
                    else if(e.getStateChange() == ItemEvent.DESELECTED){
                        dcjExtendSubsequences.setEnabled(false);
                    }

                    validate();
                    repaint();
                }
            });
        
        this.dcjPreCompute = new JCheckBox("", Parameter.DEFAULT_PRE_COMPUTE);
        this.dcjPreCompute.setToolTipText("<html>If checked, the local DCJ similarity will be computed for all clusters before selecting them in the GUI or exporting results.<br>This also enables the sorting of clusters by the average DCJ similarity in the GUI and when exporting cluster results.<br><b>*This can be highly time consuming if too many clusters are found!*</b></html>");
        this.dcjPreCompute.setBackground(new Color(0,0,0,0));
        
        this.dcjUseHeuristics = new JCheckBox("", Parameter.DEFAULT_USE_HEURISTICS);
        this.dcjUseHeuristics.setToolTipText("<html>If checked, the local DCJ similarity will be computed using heuristics<br>(useful to speedup computation when clusters have many duplicate genes).</html>");
        this.dcjUseHeuristics.setBackground(new Color(0,0,0,0));
        
        JPanel dcjSimilarityPanel = getDCJSimilarityPanel();
        
        /*
         * Tabbed distance and size pane
         */
        tabbedDistancePane = new JTabbedPane();
        tabbedDistancePane.addTab("Single Distance", getDistancePanel());
        tabbedDistancePane.addTab("Distance Table", deltaTable);
        tabbedDistancePane.addTab("DCJ Similarity", dcjSimilarityPanel);
        // Trying to have all tabs in one row
        tabbedDistancePane.setPreferredSize(tabbedDistancePane.getPreferredSize());
        tabbedDistancePane.setPreferredSize(new Dimension(tabbedDistancePane.getPreferredSize().width + 50,
                                                          tabbedDistancePane.getPreferredSize().height));
        //                                                  tabbedDistancePane.getPreferredSize().height - 10)); // this was good when the DCJ similarity tab's height was less than the distance table tab's height
        
        /*
         * All other options
         */
        dstCombo = new JComboBox<>(new String []{"Single distance","Distance table"});
        dstCombo.setSelectedIndex(0);
        
        final String[] qValues = new String[Math.max(gecko.getGenomes().length, 1)];
        qValues[0] = "all";
        for (int i = 1; i < qValues.length; i++) {
            qValues[i] = Integer.toString(i + 1);
        }
        qCombo = new JComboBox<>(qValues);
        qCombo.setSelectedIndex(0);

        refCombo = new JComboBox<>(Parameter.ReferenceType.getSupported());

        mergeResults = new JCheckBox("Merge Results");
        mergeResults.setSelected(false);

        refInRef = new JCheckBox("Search Ref. in Ref.");
        refInRef.setSelected(false);

        /*
         * Ref cluster options
         */
        final JTextField refClusterField = new JTextField() {
            private static final long serialVersionUID = 8768959243069148651L;

            @Override
            public Point getToolTipLocation(MouseEvent event) {
                Point p = this.getLocation();
                p.y = p.y + getHeight();
                return p;
            }
        };

        EventList<Genome> genomeEventList = new BasicEventList<>();
        genomeEventList.addAll(Arrays.asList(GeckoInstance.getInstance().getGenomes()));
        final JComboBox refGenomeCombo = new JComboBox();
        AutoCompleteSupport support = AutoCompleteSupport.install(refGenomeCombo, genomeEventList);
        support.setStrict(true);
        refGenomeCombo.setSelectedIndex(0);

        additionalRefClusterSettings = new JPanel(new CardLayout());
        additionalRefClusterSettings.add(new JPanel(), Parameter.ReferenceType.allAgainstAll.toString());
        additionalRefClusterSettings.add(refGenomeCombo, Parameter.ReferenceType.genome.toString());
        additionalRefClusterSettings.add(refClusterField, Parameter.ReferenceType.cluster.toString());

        /*
         * layout of other options
         */
        // Actions
        dstCombo.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int selected = dstCombo.getSelectedIndex();
                tabbedDistancePane.setEnabledAt(selected, true);
                tabbedDistancePane.setEnabledAt( (selected+1)%2, false);
                if (tabbedDistancePane.getSelectedIndex() < 2)
                    tabbedDistancePane.setSelectedIndex(selected);
            }
        });
        qCombo.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (qCombo.getSelectedIndex() == 0) {
                    quorum = 0;
                } else {
                    quorum = qCombo.getSelectedIndex() + 1;
                }
            }
        });

        refCombo.setEnabled(true);
        mergeResults.setEnabled(true);

        refClusterField.setToolTipText("<html>Enter a sequence of gene IDs here, separated by spaces.<br>(a gene ID starting with minus sign means reverse orientation in the reference sequence)<br>"
                + "<B>Hint</B>: Instead of entering a sequence of genes by hand you can also select a gene cluster from the result list and copy it!</html>");

        Document refClusterFieldDocument = new PlainDocument() {

            /**
             * Random generated serialization UID
             */
            private static final long serialVersionUID = -1398119324023579537L;

            @Override
            public void insertString(int offs, String str, AttributeSet a)
                    throws BadLocationException {;
                super.insertString(offs, str, a);
            }

        };

        refClusterField.setDocument(refClusterFieldDocument);

        refCombo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                refType = (Parameter.ReferenceType) refCombo.getSelectedItem();
                CardLayout layout = (CardLayout) (additionalRefClusterSettings.getLayout());
                layout.show(additionalRefClusterSettings, refType.toString());
                if (refType.equals(Parameter.ReferenceType.cluster)) {
                    ToolTipManager.sharedInstance().mouseMoved(
                            new MouseEvent(refClusterField, 0, 0, 0,
                                    (int) refClusterField.getLocation().getX(), // X-Y of the mouse for the tool tip
                                    (int) refClusterField.getLocation().getY(),
                                    0, false));
                }
            }
        });

        Action okAction = new AbstractAction("OK") {
            private static final long serialVersionUID = 6197096728152587585L;

            public void actionPerformed(ActionEvent e) {
                Parameter parameter;
                if (dstCombo.getSelectedIndex() == 0) {
                    parameter = new Parameter(
                            (Integer) distanceSpinner.getValue(),
                            (Integer) sizeSpinner.getValue(),
                            quorum,
                            opMode,
                            refType,
                            refInRef.isSelected(),
                            false);
                } else {
                    if (!deltaTable.isValidDeltaTable()) {
                        JOptionPane.showMessageDialog(StartComputationDialog.this, "Invalid Distance Table!", "Error", JOptionPane.ERROR_MESSAGE);
                        return;
                    } else {
                        parameter = new Parameter(
                                deltaTable.getDeltaTable(),
                                deltaTable.getClusterSize(),
                                quorum,
                                opMode,
                                refType,
                                refInRef.isSelected(),
                                false);
                    }
                }

                // Reorder the genomes if necessary
                if (opMode == Parameter.OperationMode.reference && refType == Parameter.ReferenceType.genome && refGenomeCombo.getSelectedIndex() != 0) {
                    gecko.reorderGenomes(refGenomeCombo.getSelectedIndex());
                } else if (opMode == Parameter.OperationMode.reference && refType == Parameter.ReferenceType.cluster) {
                    Genome cluster = new Genome("Reference cluster");
                    ArrayList<Gene> genes = new ArrayList<>();
                    for (String id : refClusterField.getText().split(" ")) {
                        if (id != null && (!(id.equals("")))) {
                            boolean reverse = false;
                            if (id.startsWith("-")) {
                                reverse = true;
                                id = id.substring(1);
                            }
                            if (id.startsWith("+"))
                                id = id.substring(1);
                            GeneFamily geneFamily = gecko.getGeneFamily(id);
                            if (geneFamily != null) {
                                genes.add(new Gene(geneFamily, reverse ? Gene.GeneOrientation.NEGATIVE : Gene.GeneOrientation.POSITIVE));
                            } else {
                                JOptionPane.showMessageDialog(StartComputationDialog.this, "Invalid Gene Id: " + id, "Error", JOptionPane.ERROR_MESSAGE);
                                return;
                            }
                        }
                    }
                    cluster.addChromosome(new Chromosome("", genes, cluster));
                    gecko.addReferenceGenome(cluster);
                }
                
                parameter.setDCJParameters((double) dcjDiscardedPenality.getValue(),
                                           (int) dcjBorderlineCycleLength.getValue(), 
                                           dcjComputeSubintervals.isSelected(),
                                           (int) dcjExtendSubsequences.getValue(),
                                           dcjPreCompute.isSelected(),
                                           dcjUseHeuristics.isSelected());
                
                saveToPreferences(gecko.getPreferences());
                StartComputationDialog.this.setVisible(false);
                boolean mergeResultsEnabled = false;
                if (opMode == Parameter.OperationMode.reference && mergeResults.isSelected()) {
                    mergeResultsEnabled = true;
                }

                gecko.performClusterDetection(
                        parameter,
                        mergeResultsEnabled,
                        1.1);
            }

        };
        JButton okButton = new JButton(okAction);

        Action cancelAction = new AbstractAction() {
            private static final long serialVersionUID = 2057638030083370800L;

            public void actionPerformed(ActionEvent e) {
                StartComputationDialog.this.setVisible(false);
            }

        };
        cancelAction.putValue(Action.NAME, "Cancel");
        JButton cancelButton = new JButton(cancelAction);
        JPanel buttonPanel = new ButtonBarBuilder().addButton(okButton, cancelButton).build();

        FormLayout layout = new FormLayout("center:default", "default:grow, default, default");
        DefaultFormBuilder builder = new DefaultFormBuilder(layout);
        builder.append(tabbedDistancePane);
        builder.nextLine();
        builder.append(getBody());
        builder.nextLine();
        builder.append(buttonPanel);

        JPanel panel = builder.build();
        panel.getActionMap().put("okAction", okAction);
        panel.getActionMap().put("cancelAction", cancelAction);
        panel.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), "okAction");
        panel.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "cancelAction");

        this.setContentPane(panel);
        this.pack();
        this.loadFromPreferences(gecko.getPreferences());
    }

    private void setParameters(Parameter parameters) {
        // Distance and size
        if (parameters.useDeltaTable()) {
            tabbedDistancePane.setSelectedIndex(1);
            tabbedDistancePane.setEnabledAt(0, false);
            tabbedDistancePane.setEnabledAt(1, true);
            dstCombo.setSelectedIndex(1);
            deltaTable.setDeltaValues(parameters.getDeltaTable(), parameters.getMinClusterSize());
        } else {
            tabbedDistancePane.setSelectedIndex(0);
            tabbedDistancePane.setEnabledAt(0, true);
            tabbedDistancePane.setEnabledAt(1, false);
            dstCombo.setSelectedIndex(0);
            distanceSpinner.setValue(parameters.getDelta());

            sizeSpinner.setValue(parameters.getMinClusterSize());
        }

        // Mode, all-against-all, genome or cluster
        if (parameters.getRefType().equals(Parameter.ReferenceType.cluster) || parameters.getRefType().equals(Parameter.ReferenceType.genome)) {
            refCombo.setSelectedItem(Parameter.ReferenceType.genome);
        } else {
            refCombo.setSelectedItem(parameters.getRefType());
        }

        // Quorum
        if (parameters.getQ() == 0) {
            qCombo.setSelectedIndex(0);
        } else {
            qCombo.setSelectedIndex(parameters.getQ() - 1);
        }

        // Ref in Ref
        refInRef.setSelected(parameters.searchRefInRef());

        dcjDiscardedPenality.setValue(parameters.getDiscardedPenality());
        dcjBorderlineCycleLength.setValue(parameters.getBorderlineLength());
        if (parameters.computeSubintervals() != dcjComputeSubintervals.isSelected())
            dcjComputeSubintervals.doClick();
        dcjExtendSubsequences.setValue(parameters.getExtendSubsequences());
        if (parameters.preCompute() != dcjPreCompute.isSelected())
            dcjPreCompute.doClick();
        if (parameters.useHeuristics()!= dcjUseHeuristics.isSelected())
            dcjUseHeuristics.doClick();
    }

    private JComponent getBody() {
        FormLayout layout = new FormLayout(
                "pref:grow, 4dlu, pref:grow",
                "p, 2dlu, p, 2dlu, p, 2dlu, p, 2dlu, p"
        );
        PanelBuilder builder = new PanelBuilder(layout);
        CellConstraints cc = new CellConstraints();

        builder.addLabel("Distance mode:", cc.xy(1, 1));
        builder.add(dstCombo, cc.xy(3, 1));
        
        builder.addLabel("Minimum # of genomes:", cc.xy(1, 3));
        builder.add(qCombo, cc.xy(3, 3));

        builder.addLabel("Reference type:", cc.xy(1, 5));
        builder.add(refCombo, cc.xy(3, 5));

        builder.add(additionalRefClusterSettings, cc.xyw(1, 7, 3));

        //builder.add(mergeResults, cc.xy(1, 9));
        builder.add(refInRef, cc.xy(1, 9));

        return builder.getPanel();
    }

    private JPanel getDistancePanel() {
        FormLayout layout = new FormLayout(
                "pref:grow, 4dlu, pref:grow",
                ""
        );
        DefaultFormBuilder builder = new DefaultFormBuilder(layout);

        builder.append("Maximum distance (\u03B4):", distanceSpinner);
        builder.append("<html>Minimum cluster size (<i>s</i>):</html>", sizeSpinner);

        return builder.getPanel();
    }

    private JPanel getDCJSimilarityPanel() {
        FormLayout layout = new FormLayout(
                "pref:grow, 4dlu, pref:grow",
                ""
        );
        DefaultFormBuilder builder = new DefaultFormBuilder(layout);

        String fontStart = "<font face=\"serif\"><i>"; // math font
        String fontEnd = "</i></font>";
        
        JLabel label = new JLabel("Penality for discarding a gene:");
        label.setToolTipText(dcjDiscardedPenality.getToolTipText());
        builder.append(label, dcjDiscardedPenality);
        
        label = new JLabel("<html>Borderline cycle length (" + fontStart + "b" + fontEnd + "):</html>");
        label.setToolTipText(dcjBorderlineCycleLength.getToolTipText());
        builder.append(label, dcjBorderlineCycleLength);
        
        label = new JLabel("Compute for subintervals:");
        label.setToolTipText(dcjComputeSubintervals.getToolTipText());
        builder.append(label);
        builder.add(dcjComputeSubintervals, new CellConstraints().xyw(builder.getColumn(), builder.getRow(), 1, CellConstraints.CENTER, CellConstraints.DEFAULT));
        builder.nextLine();
        
        label = new JLabel("Extend beyond subsequence limits:");
        label.setToolTipText(dcjExtendSubsequences.getToolTipText());
        builder.append(label, dcjExtendSubsequences);
        
        label = new JLabel("Pre-compute:");
        label.setToolTipText(dcjPreCompute.getToolTipText());
        builder.append(label);
        builder.add(dcjPreCompute, new CellConstraints().xyw(builder.getColumn(), builder.getRow(), 1, CellConstraints.CENTER, CellConstraints.DEFAULT));
        builder.nextLine();
        
        label = new JLabel("Use heuristics:");
        label.setToolTipText(dcjUseHeuristics.getToolTipText());
        builder.append(label);
        builder.add(dcjUseHeuristics, new CellConstraints().xyw(builder.getColumn(), builder.getRow(), 1, CellConstraints.CENTER, CellConstraints.DEFAULT));
        
        
        CellConstraints c = new CellConstraints();
        String cycleScoreFormula = "<html><table><tr><td rowspan=\"2\">" + 
                fontStart + "f(&#8467;) = " + fontEnd +
                "</td><td style=\"border-bottom: 1px solid; vertical-align: bottom;\"> " +
                fontStart + "2 &minus; &#8467;" + fontEnd +
                "</td><td rowspan=\"2\">" + 
                fontStart + "+ 1" + fontEnd +
                "</td></tr><tr><td style=\"vertical-align: top;\">" +
                fontStart + "b &minus; 2" + fontEnd +
                "</td><td></td></tr></table></html>";
        JLabel formulaLabel = new JLabel(cycleScoreFormula);
        formulaLabel.setToolTipText("<html>Score for a cycle, given its length &#8467;.</html>");
        builder.appendRow("center:pref:grow");
        builder.nextLine();
        builder.add(formulaLabel, c.xyw(builder.getColumn(), builder.getRow(), 3, CellConstraints.CENTER, CellConstraints.DEFAULT));
        
        return builder.getPanel();
    }
    
    private void saveToPreferences(Preferences preferences) {
        if (preferences != null) {
            preferences.putInt("delta", (int) distanceSpinner.getValue());
            preferences.putInt("deltaSize", (int) sizeSpinner.getValue());
            preferences.put("deltaTable", Arrays.deepToString(deltaTable.getDeltaTable()));
            preferences.putInt("deltaTableSize", deltaTable.getClusterSize());
            preferences.putInt("distancePaneSelected", tabbedDistancePane.getSelectedIndex());
            preferences.putInt("distanceMode", dstCombo.getSelectedIndex());
            preferences.putInt("quorum", quorum);
            preferences.put("refType", Character.toString(refType.getCharMode()));
            preferences.put("operationMode", Character.toString(opMode.getCharMode()));
            preferences.putBoolean("refInRef", refInRef.isSelected());
            
            // DCJ similarity
            preferences.putDouble("discardedPenality", (double) dcjDiscardedPenality.getValue());
            preferences.putInt("borderlineLength", (int) dcjBorderlineCycleLength.getValue());
            preferences.putBoolean("computeSubintervals", dcjComputeSubintervals.isSelected());
            preferences.putInt("extendSubsequences", (int) dcjExtendSubsequences.getValue());
            preferences.putBoolean("preCompute", dcjPreCompute.isSelected());
            preferences.putBoolean("useHeuristics", dcjUseHeuristics.isSelected());
        }
    }

    private void loadFromPreferences(Preferences preferences) {
        if (preferences != null) {
            // Load from preferences
            int delta = preferences.getInt("delta", 3);
            int deltaSize = preferences.getInt("deltaSize", 7);

            String deltaTableString = preferences.get("deltaTable", "null");
            int[][] deltaTable = null;
            if (!deltaTableString.equals("null")) {
                try {
                    String[] split = deltaTableString.replaceFirst("^\\[*", "").replaceAll("\\]*$", "").split("\\],\\s*\\[");
                    deltaTable = new int[split.length][];
                    for (int i = 0; i < split.length; i++) {
                        String[] values = split[i].split(",");
                        if (values.length != Parameter.DELTA_TABLE_SIZE) {
                            deltaTable = null;
                            logger.warn("Invalid delta table size at {}", split[i]);
                            break;
                        }
                        deltaTable[i] = new int[values.length];
                        for (int j = 0; j < values.length; j++) {
                            deltaTable[i][j] = Integer.parseInt(values[j].trim());
                        }
                    }
                } catch (NumberFormatException e) {
                    deltaTable = null;
                    logger.warn("Invalid deltaTable format, could not read the data", e);
                }
            }
            int deltaTableSize = preferences.getInt("deltaTableSize", Parameter.DeltaTable.getDefault().getMinimumSize());
            if (deltaTable == null) {
                deltaTable = Parameter.DeltaTable.getDefault().getDeltaTable();
                deltaTableSize = Parameter.DeltaTable.getDefault().getMinimumSize();
            }
            int distancePaneSelected = preferences.getInt("distancePaneSelected", 0);
            int q = preferences.getInt("quorum", 0);
            Parameter.ReferenceType referenceType = Parameter.ReferenceType.getReferenceTypeFromChar(preferences.get("refType", "a").charAt(0));
            Parameter.OperationMode operationMode = Parameter.OperationMode.getOperationModeFromChar(preferences.get("operationMode", "r").charAt(0));
            boolean searchRefInRef = preferences.getBoolean("refInRef", false);

            // Set values
            // Distance and size
            tabbedDistancePane.setSelectedIndex(distancePaneSelected < tabbedDistancePane.getTabCount() ? distancePaneSelected : 0);
            dstCombo.setSelectedIndex(preferences.getInt("distanceMode", 0));
            this.deltaTable.setDeltaValues(deltaTable, deltaTableSize);
            distanceSpinner.setValue(delta);
            sizeSpinner.setValue(deltaSize);

            this.opMode = operationMode;
            // Mode, all-against-all, genome or cluster
            if (referenceType.equals(Parameter.ReferenceType.cluster) || referenceType.equals(Parameter.ReferenceType.genome)) {
                refCombo.setSelectedItem(Parameter.ReferenceType.genome);
            } else {
                refCombo.setSelectedItem(referenceType);
            }

            // Quorum
            if (q == 0 || q >= qCombo.getItemCount()) {
                qCombo.setSelectedIndex(0);
            } else {
                qCombo.setSelectedIndex(q - 1);
            }

            // Ref in Ref
            refInRef.setSelected(searchRefInRef);
            
            // DCJ similarity
            dcjDiscardedPenality.setValue(preferences.getDouble("discardedPenality", Parameter.DEFAULT_DISCARDED_PENALITY));
            dcjBorderlineCycleLength.setValue(preferences.getInt("borderlineLength", Parameter.DEFAULT_BORDERLINE_LENGTH));
            dcjComputeSubintervals.setSelected(preferences.getBoolean("computeSubintervals", Parameter.DEFAULT_COMPUTE_SUBINTERVALS));
            dcjExtendSubsequences.setValue(preferences.getInt("extendSubsequences", Parameter.DEFAULT_EXTEND_SUBSEQUENCES));
            dcjPreCompute.setSelected(preferences.getBoolean("preCompute", Parameter.DEFAULT_PRE_COMPUTE));
            dcjUseHeuristics.setSelected(preferences.getBoolean("useHeuristics", Parameter.DEFAULT_USE_HEURISTICS));
        }
    }

}
