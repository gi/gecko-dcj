/*
 * Copyright 2019 Diego Rubert
 *
 * This file is part of Gecko3.
 *
 * Gecko3 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gecko3 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Gecko3.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Progress bar showed during the calculation of the local DCJ similarity of
 * all clusters to write results or pre-compute the similarity for all clusters
 * @author Diego Rubert
 */


package de.unijena.bioinf.gecko3.gui;

import de.unijena.bioinf.gecko3.gui.util.DisabledGlassPane;
import de.unijena.bioinf.gecko3.util.DCJSimilarityTaskCluster;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.beans.*;
import javax.swing.text.DefaultCaret;


public class DCJProgressBarWindowAllClusters extends JFrame {

    private final DCJProgressBarAllClusters progressBar;
    private final int numberOfTasks;
    
    public DCJProgressBarWindowAllClusters(int numberOfTasks, JRootPane rootPane) {
        super("Computing local DCJ similarity...");

        this.numberOfTasks = numberOfTasks;
        
        if (numberOfTasks == 0) {
            this.progressBar = null;
            dispose();
            return;
        }
        
        this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        this.progressBar = new DCJProgressBarAllClusters(numberOfTasks, this, rootPane); // we need the root pane to disable it
        this.progressBar.setOpaque(true); //content panes must be opaque
        this.setContentPane(this.progressBar);
        this.setResizable(false);
        this.setIconImage(new ImageIcon(ClassLoader.getSystemResource("images/computing.png")).getImage());
        this.pack();
    }
    
    public void setVisible(boolean b) {
        if (numberOfTasks == 0) 
            return;
        super.setVisible(b);
    }

    /*
     * Appends some result text to the task output text panel
     */
    public void appendResultText(String text) {
        progressBar.appendResultText(text);
    }
    
    /*
     * Adds and run another task (blocking)
     */
    public boolean addTask(DCJSimilarityTaskCluster task, int numberOfSubtasks) {
        boolean canceled = !progressBar.addTask(task);
        if (canceled)
            progressBar.end();
        return !canceled;
    }
    

    private class DCJProgressBarAllClusters extends JPanel
            implements ActionListener,
            PropertyChangeListener {

        private final JProgressBar progressBar;
        private final JProgressBar progressBarSub;
        private final JButton cancelButton;
        private final JTextArea taskOutput;
        private final DisabledGlassPane glassPane;
        private final JFrame progressBarWindow;
        private DCJSimilarityTaskCluster task;
        private final int numberOfTasks;

        private int computedTasks;
        
        private boolean cancelled;
                
        public DCJProgressBarAllClusters(int numberOfTasks, JFrame progressBarWindow, JRootPane rootPane) {
            super(new BorderLayout());
            
            this.numberOfTasks = numberOfTasks;
            this.computedTasks = 0;
            this.cancelled = false;

            this.glassPane = new DisabledGlassPane();
            rootPane.setGlassPane(glassPane); // set the glasspane for the rootpane (of the main application)
            this.progressBarWindow = progressBarWindow; // save the window (JFrame) so we can close it later

            cancelButton = new JButton("Cancel");
            cancelButton.setActionCommand("cancel");
            cancelButton.addActionListener(this);

            progressBar = new JProgressBar(0, numberOfTasks);
            progressBar.setValue(0);
            progressBar.setStringPainted(true);
            progressBar.setBorder(BorderFactory.createEmptyBorder(0,0,0,0));
            
            progressBarSub = new JProgressBar(0, 100);
            progressBarSub.setValue(0);
            progressBarSub.setStringPainted(true);
            progressBarSub.setBorder(BorderFactory.createEmptyBorder(0,0,0,0));
            
            JPanel panelBars = new JPanel();
            panelBars.setLayout(new BoxLayout(panelBars, BoxLayout.Y_AXIS));
            panelBars.add(new JLabel("Clusters:"));
            panelBars.add(progressBar);
            panelBars.add(Box.createRigidArea(new Dimension(0,5)));
            panelBars.add(new JLabel("Computations for the current cluster:"));
            panelBars.add(progressBarSub);

            taskOutput = new JTextArea(10, 50);
            taskOutput.setMargin(new Insets(5, 5, 5, 5));
            taskOutput.setEditable(false);
            JScrollPane taskOutputScroll = new JScrollPane(taskOutput);
            DefaultCaret caret = (DefaultCaret)taskOutput.getCaret();
            caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);

            this.setLayout(new BorderLayout(5,5));
            
            add(panelBars, BorderLayout.PAGE_START);
            add(taskOutputScroll, BorderLayout.CENTER);
            add(cancelButton, BorderLayout.PAGE_END);
            setBorder(BorderFactory.createEmptyBorder(10, 15, 10, 15));
            
            begin();
        }
        
        /*
         * Adds and run another task (blocking), returns true if task finished
         */
        public boolean addTask(DCJSimilarityTaskCluster task) {
            if (this.cancelled)
                return false;
            
            this.task = task;
            
            progressBarSub.setValue(0);
            progressBarSub.setMaximum(task.getNumberOfSubtasks());
            
            appendResultText("Cluster ID " + task.getCluster().getId() + " (" + (computedTasks+1) + "/" + numberOfTasks + ")");
            
            task.addPropertyChangeListener(this);
            task.execute();
            
            waitDCJComputationTask();
            computedTasks++;
            progressBar.setValue(computedTasks);
            
            if (computedTasks == numberOfTasks)
                end();
                        
            return !task.isCancelled();
        }

        /*
         * Cancel button pressed
         */
        @Override
        public void actionPerformed(ActionEvent evt) {
            task.cancel(true);
            this.cancelled = true;
        }

        /*
         * Progress property changed
         */
        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            if ("progress".equals(evt.getPropertyName())) {
                int progress = task.getComputedSubtasks();
                progressBarSub.setValue(progress);
                //(Integer) evt.getNewValue(); // this would return an percentage, not the count of subtasks done
                // task.getProgress() // this would return the percentage too                progressBarSub.setValue(progress);
            }
        }
        
        /*
         * Appends some result text to the task output text panel
         */
        private void appendResultText(String text) {
            taskOutput.append(text);
            taskOutput.append("\n");
        }
        
        /*
         * Prepares the window to show computation
         */
        private void begin() {
            glassPane.activate();
            setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        }
        
        /*
         * Returns things as they were before starting computation
         */
        private void end() {
            setCursor(null); //turn off the wait cursor
            glassPane.deactivate();
            progressBarWindow.setVisible(false);
            progressBarWindow.dispose();
        }
        
        /*
         * Waits the DCJ computation task to finish
         */
        private void waitDCJComputationTask() {
            while(!task.isDone()) {
                Thread.yield();
            }
        }                
        
    }

}