/*
 * Copyright 2019 Diego Rubert
 *
 * This file is part of Gecko3.
 *
 * Gecko3 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gecko3 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Gecko3.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Panel that displays for a single sequence the DCJ similarity and opens a window with more information on click
 * @author Diego Rubert
 */

package de.unijena.bioinf.gecko3.gui;

import de.unijena.bioinf.gecko3.GeckoInstance;
import de.unijena.bioinf.gecko3.algo.DCJSimilarity;
import de.unijena.bioinf.gecko3.datastructures.Subsequence;
import de.unijena.bioinf.gecko3.datastructures.Chromosome;
import de.unijena.bioinf.gecko3.datastructures.Parameter;
import de.unijena.bioinf.gecko3.datastructures.util.DCJSimilarityData;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.text.DecimalFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;


public class DCJSimilarityPanel extends JPanel implements MouseListener {

    private final Subsequence seq;
    private final Subsequence refseq;
    private final Chromosome chr;
    private final Chromosome refchr;
    private final DCJSimilarityData dcjSimData;
    private final Parameter parameters;
    
    private JFrame agWindow;

    private static final FlowLayout LAYOUT = new FlowLayout(FlowLayout.LEFT, 5, 1);

    public DCJSimilarityPanel(int number, Subsequence seq, Subsequence refseq, Chromosome chr, Chromosome refchr, Font font, Color background, Parameter parameters) {
        super(LAYOUT);

        this.seq = seq;
        this.refseq = refseq;
        this.chr = chr;
        this.refchr = refchr;
        this.dcjSimData = seq.getDCJSim(refseq);
        this.parameters = parameters;
        this.agWindow = null;

        String tooltip = "<html>DCJ Similarity not computed.</html>";
        
        if (dcjSimData != null && dcjSimData.noSimilarity())
            tooltip = "<html>No similarity found.</html>";
        
        if (dcjSimData != null && !dcjSimData.noSimilarity()) {
            //BigDecimal pvalue = seq.getpValue(); // if we multiply all p-values (not p-scores) we will have the value shown in the Score row in clusters list
            tooltip = "<html>";
            if (!parameters.computeSubintervals())
                tooltip += String.format("DCJ similarity: %g<br>", dcjSimData.sim());
            else { // compute subintervals is on
                tooltip += String.format("Best DCJ similarity: %g<br>", dcjSimData.sim());
                tooltip += String.format("&nbsp;&nbsp;&middot; refseq. subinterval: [%d-%d]<br>", dcjSimData.startA(), dcjSimData.stopA());
                tooltip += String.format("&nbsp;&nbsp;&middot; seq. subinterval: [%d-%d]<br>", dcjSimData.startB(), dcjSimData.stopB());
            }
            tooltip += String.format("&nbsp;&nbsp;&middot; cycles: %d<br>", dcjSimData.cycles());
            tooltip += String.format("&nbsp;&nbsp;&middot; odd-paths: %d<br>", dcjSimData.odd());
            tooltip += String.format("&nbsp;&nbsp;&middot; even-paths: %d<br>", dcjSimData.even());
            tooltip += String.format("&nbsp;&nbsp;&middot; discarded genes: %d<br>", dcjSimData.discarded());
            tooltip += String.format("&nbsp;&nbsp;&middot; DCJs to sort: %d<br>", dcjSimData.DCJsToSort());
            tooltip += String.format("<i>p</i>-score: %g", seq.getScore());
            tooltip += "</html>";
        }

        setBackground(background);
        setBorder(BorderFactory.createEmptyBorder());
        setToolTipText(tooltip);

        JLabel numberPanel = new NumberInRectangle(number, background, this);
        numberPanel.setToolTipText(tooltip);
        add(numberPanel);

        JLabel textLabel;
        if (dcjSimData == null)
            textLabel = new JLabel("?");
        else
            textLabel = new JLabel(dcjSimData.noSimilarity() ? "\u2013" : new DecimalFormat("#.##").format(dcjSimData.sim()));
        textLabel.setFont(font);
        add(textLabel);
        addMouseListener(this);
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        
        if (dcjSimData == null || dcjSimData.noSimilarity())
            return;
        
        // If ag is null (not saved) we have to calculate the dcj similarity again,
        // because of de-duplication and etc, we cannot infer the adjacency graph (at least
        // we can reuse the start/stop A/B to calculate for the right interval without considering subintervals        
        if (dcjSimData.ag() == null) {
            DCJSimilarityData dcjSimDataTmp;
            DCJSimilarity dcjSim = new DCJSimilarity(parameters);
            try {
                dcjSimDataTmp = dcjSim.computeDCJSimilaritySubInterval(refseq, refchr, dcjSimData.startA(), dcjSimData.stopA(), seq, chr, dcjSimData.startB(), dcjSimData.stopB());
            } catch (InterruptedException ex) {
                JOptionPane.showMessageDialog(null, "Could not compute the local DCJ similarity for this sequence!\nError:\n" + ex, "DCJ similarity computation error", JOptionPane.ERROR_MESSAGE);
                return;
            }
            dcjSimData.setAg(dcjSimDataTmp.ag());
        }

        //TODO: remove
        System.out.println("Original ref: " + refseq);
        System.out.println("Original seq: " + seq);
        System.out.println(dcjSimData.ag());
        
        if (agWindow == null)
            agWindow = new AdjacencyGraphWindow(seq.getDCJSim(refseq).ag());
        if (agWindow.isVisible())
            agWindow.toFront();
        else {
            agWindow.setVisible(true);
            agWindow.setLocation(e.getLocationOnScreen().x - agWindow.getWidth()/2, e.getLocationOnScreen().y - agWindow.getHeight()/2);
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        if (dcjSimData != null && !dcjSimData.noSimilarity())
            setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        GeckoInstance.getInstance().getGui().setInfobarText(chr.getFullName());
    }

    @Override
    public void mouseExited(MouseEvent e) {
        setCursor(null);
        GeckoInstance.getInstance().getGui().setInfobarText("");
    }

}
