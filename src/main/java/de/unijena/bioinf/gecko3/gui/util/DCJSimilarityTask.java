/*
 * Copyright 2019 Diego Rubert
 *
 * This file is part of Gecko3.
 *
 * Gecko3 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gecko3 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Gecko3.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Thread for computing the DCJSimilarity on GUI
 * @author Diego Rubert
 */

package de.unijena.bioinf.gecko3.gui.util;

import de.unijena.bioinf.gecko3.algo.DCJSimilarity;
import de.unijena.bioinf.gecko3.datastructures.Chromosome;
import de.unijena.bioinf.gecko3.datastructures.GeneCluster;
import de.unijena.bioinf.gecko3.datastructures.Parameter;
import de.unijena.bioinf.gecko3.datastructures.Subsequence;
import de.unijena.bioinf.gecko3.datastructures.util.DCJSimilarityData;
import java.util.List;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import javax.swing.SwingWorker;


public class DCJSimilarityTask extends SwingWorker<Void, String> {

    private final GeneCluster cluster;
    private final List<Subsequence> subsequences;
    private final List<Chromosome> chromosomes;
    private final Parameter parameters;
    private int progress; // the number of subtasks computed

    public DCJSimilarityTask(GeneCluster cluster, List<Subsequence> subsequences, List<Chromosome> chromosomes, Parameter parameters) {
        super();

        this.cluster = cluster;
        this.subsequences = subsequences;
        this.chromosomes = chromosomes;
        this.parameters = parameters;
    }
    
    /*
     * Returns the internal counter of subtasks computed
     */
    public int getComputedSubtasks() {
        return this.progress;
    }
    
    /*
     * Calls the local DCJ similarity computation for each sequence and updates progress
     */
    @Override
    public Void doInBackground() {
        int refseqindex = cluster.getRefSeqIndex();
        Subsequence refseq = subsequences.get(refseqindex);
        Chromosome refchr = chromosomes.get(refseqindex);
        
        //System.out.println(cluster);
        //System.out.print("Reference: ");
        //System.out.println(refseq);
        DCJSimilarity dcj = new DCJSimilarity(parameters, this);
        DCJSimilarityData dcjSimData;
        
        progress = 0;
        setProgress(0);
        //System.out.println("refseqindex: " + refseqindex);
        while (progress < subsequences.size()) {
            try {
                Subsequence seq = subsequences.get(progress);
                Chromosome chr = chromosomes.get(progress);
                if (seq.getDCJSim(refseq) == null) { // if the similarity to the current refseq was never computed for this sequence
                    dcjSimData = dcj.computeDCJSimilarity(refseq, refchr, seq, chr);
                    if (dcjSimData != null) {
                        seq.addDCJSim(refseq, dcjSimData);
                        // New label: Adjacency Graph Cluster ID cluster_Id refseq_nr[interval] seq_nr[interval]
                        if (dcjSimData.ag() != null)
                            dcjSimData.ag().setLabel(String.format("Cluster ID %d %d[%d-%d] %d[%d-%d]", 
                                                                    cluster.getId(),
                                                                    refseqindex,
                                                                    dcjSimData.startA(),
                                                                    dcjSimData.stopA(),
                                                                    progress,
                                                                    dcjSimData.startB(),
                                                                    dcjSimData.stopB()));
                    }
                    else
                        seq.addDCJSim(refseq, DCJSimilarityData.noSimilarity(refseq, seq));
                    //Thread.sleep(500);
                }
            } catch (InterruptedException e) {
                // System.out.println("DCJ computation canceled (1)!!!");
            }
            if (this.isCancelled()) {
                // System.out.println("DCJ computation canceled (2)!!!!");
                return null;
            }
            progress++;
            setProgress((int) (100 * progress / (float) subsequences.size()));
        }
        //try { Thread.sleep(200); } catch (InterruptedException ignore) { }
        return null;
    }

    /*
     * Executed in event dispatching thread
     */
    @Override
    public void done() {
        try {
            get(); // we do get() only to get an stacktrace if an exception occurred
        } catch (InterruptedException | CancellationException ex) {
            // no problem, the user can cancel the task
            // System.out.println("DCJ computation canceled (3)!!!!");
        } catch (ExecutionException ex) {
            System.out.println("Exception: " + ex);
            throw new RuntimeException(ex.getCause());
        }
        super.done();
    }
}
