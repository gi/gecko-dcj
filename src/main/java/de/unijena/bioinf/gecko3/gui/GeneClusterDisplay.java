/*
 * Copyright 2019 Diego Rubert
 * Copyright 2014 Sascha Winter, Tobias Mann, Hans-Martin Haase, Leon Kuchenbecker and Katharina Jahn
 *
 * This file is part of Gecko3.
 *
 * Gecko3 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gecko3 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Gecko3.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.unijena.bioinf.gecko3.gui;


import de.unijena.bioinf.gecko3.GeckoInstance;
import de.unijena.bioinf.gecko3.datastructures.*;
import de.unijena.bioinf.gecko3.event.ClusterSelectionEvent;
import de.unijena.bioinf.gecko3.event.ClusterSelectionListener;
import de.unijena.bioinf.gecko3.event.LocationSelectionEvent;
import de.unijena.bioinf.gecko3.gui.util.DCJSimilarityTask;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumnModel;
import java.awt.*;
import java.util.*;
import java.util.List;


public class GeneClusterDisplay extends JScrollPane implements ClusterSelectionListener {

	private static final long serialVersionUID = -2156280340296694286L;

    private static final Font monoFont = new Font("Monospaced",Font.PLAIN,new JLabel().getFont().getSize()-1);
    private static final Font boldFont = new JLabel().getFont().deriveFont(Font.BOLD);

    private final JPanel masterPanel;

    private GeneCluster cluster;
    private Parameter parameters;
    private boolean includeSubOptimalOccurrences;
    private int[] subselections;

    private String maxLengthString;
    private int geneWidth;

    private List<Subsequence> subsequences;
    private List<Chromosome> chromosomes;
    private List<Integer> genomeIndexMapping;
    private Map<Integer, Integer> genomeIndexBackmap;

    private List<Gene> geneList;
    private List<Integer> genomeIndexInGeneList;
    private Map<Integer, GeneFamily> geneIdAtTablePosition;
    private List<ReferenceGeneInfo> referenceGeneList;

    // local?
    private final JTable chromosomeNameTable;
    private final JTable referenceGeneTable;
    private final JTable annotationTable;
    // end local?
	
	public GeneClusterDisplay() {
		masterPanel = new JPanel();		
		masterPanel.setLayout(new BoxLayout(masterPanel, BoxLayout.PAGE_AXIS));
        masterPanel.setAlignmentX(LEFT_ALIGNMENT);
		masterPanel.setBackground(Color.WHITE);
		this.setViewportView(masterPanel);

        chromosomeNameTable = new JTable(new ChromosomeNameTableModel());
        chromosomeNameTable.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
        chromosomeNameTable.setShowGrid(false);
        chromosomeNameTable.setAlignmentX(Component.LEFT_ALIGNMENT);
        chromosomeNameTable.setDefaultRenderer(NumberInRectangle.NumberIcon.class, new NumberIconRenderer());
        final TableColumnModel chromosomeNameTableColumnModel = chromosomeNameTable.getColumnModel();
        chromosomeNameTableColumnModel.getColumn(0).setPreferredWidth(50); // Index
        chromosomeNameTableColumnModel.getColumn(0).setMaxWidth(50); // Index
        chromosomeNameTableColumnModel.getColumn(1).setPreferredWidth(200);

        referenceGeneTable = new JTable(new ReferenceGeneTableModel());
        referenceGeneTable.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
        referenceGeneTable.setShowGrid(false);
        referenceGeneTable.setAlignmentX(Component.LEFT_ALIGNMENT);
        referenceGeneTable.setDefaultRenderer(GeneFamily.class, new GeneRenderer());
        referenceGeneTable.getTableHeader().setReorderingAllowed(false);
        referenceGeneTable.getTableHeader().setFont(referenceGeneTable.getTableHeader().getFont().deriveFont(10.0f));
        final TableColumnModel referenceTableColumnModel = referenceGeneTable.getColumnModel();
        referenceTableColumnModel.getColumn(1).setPreferredWidth(65); // Index
        referenceTableColumnModel.getColumn(1).setMaxWidth(65); // Index
        referenceTableColumnModel.getColumn(2).setPreferredWidth(200);

        annotationTable = new JTable(new GeneAnnotationTableModel());
        annotationTable.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
        annotationTable.setShowGrid(false);
        annotationTable.setAlignmentX(Component.LEFT_ALIGNMENT);
        annotationTable.setDefaultRenderer(NumberInRectangle.NumberIcon.class, new NumberIconRenderer());
        annotationTable.setDefaultRenderer(GeneFamily.class, new GeneRenderer());
        final TableColumnModel annotationTableColumnModel = annotationTable.getColumnModel();
        annotationTableColumnModel.getColumn(1).setPreferredWidth(50); // Index
        annotationTableColumnModel.getColumn(1).setMaxWidth(50); // Index
        annotationTableColumnModel.getColumn(2).setPreferredWidth(200);

        reset();

        computeDCJandUpdate();
    }

    private void reset() {
        subsequences = new ArrayList<>();
        chromosomes = new ArrayList<>();
        genomeIndexMapping = new ArrayList<>();
        genomeIndexBackmap = new HashMap<>();

        geneList = new ArrayList<>();
        genomeIndexInGeneList = new ArrayList<>();
        geneIdAtTablePosition = new HashMap<>();
        includeSubOptimalOccurrences = false;
        subselections = null;
        referenceGeneList = new ArrayList<>();
    }

    @Override
    public void selectionChanged(ClusterSelectionEvent e) {
        if (!(e instanceof LocationSelectionEvent)) {
            return;
        }
        LocationSelectionEvent l = (LocationSelectionEvent) e;

        GeckoInstance gecko = GeckoInstance.getInstance();

        cluster = l.getSelection();
        parameters = gecko.getParameters();

        reset();

        if (cluster != null) {
            subselections = l.getSubselection();
            includeSubOptimalOccurrences = l.includeSubOptimalOccurrences();

            this.setGeneData();

            for (int i = 0; i < subselections.length; i++) {
                if (subselections[i] != GeneClusterOccurrence.GENOME_NOT_INCLUDED && cluster.getOccurrences(includeSubOptimalOccurrences).getSubsequences()[i][subselections[i]].isValid()) {
                    Subsequence subseq = cluster.getOccurrences(includeSubOptimalOccurrences).getSubsequences()[i][subselections[i]];
                    subsequences.add(subseq);
                    chromosomes.add(gecko.getGenomes()[i].getChromosomes().get(subseq.getChromosome()));
                    genomeIndexBackmap.put(i, genomeIndexMapping.size());
                    genomeIndexMapping.add(i);
                }
            }
        }

        computeDCJandUpdate();
    }

    private void setMaxLengthStringWidth(int idLength){
        if (maxLengthString == null || maxLengthString.length() != idLength){
            maxLengthString = GenomePainting.buildMaxLengthString(idLength);
        }
        geneWidth = GenomePainting.getGeneWidth(masterPanel.getGraphics(), maxLengthString, MultipleGenomesBrowser.DEFAULT_GENE_HEIGHT);
    }

    private void computeDCJandUpdate() {
        if (geneList.size() != 0) {
            Window mainWindow = SwingUtilities.windowForComponent(masterPanel);
            
            //Create and set up the DCJ similarity calculation progress bar window
            DCJSimilarityTask task = new DCJSimilarityTask(cluster, subsequences, chromosomes, parameters);
            final DCJProgressBarWindow progressBarWindow = new DCJProgressBarWindow(chromosomes, task, subsequences.size(), SwingUtilities.getRootPane(masterPanel));
            progressBarWindow.setVisible(true);
            progressBarWindow.setLocation(mainWindow.getX()+(mainWindow.getWidth()-progressBarWindow.getWidth())/2,
                                      mainWindow.getY()+(mainWindow.getHeight()-progressBarWindow.getHeight())/2);
            
            new Thread(new Runnable() {
                public void run() {
                    progressBarWindow.waitDCJComputationTask(); // we can compute in another thread...
                    SwingUtilities.invokeLater(new Runnable() { // however, we must ask the update directely
                        public void run() {                     // on the Event Dispatch Thread (EDT), which
                            update();                           // is taken care by the invokeLater, otherwise
                        }                                       // the thread concurrency can break the components
                    });                                         // by interfering in their managment... In short,
                }                                               // we should't add/remove components in another thread,
            }).start();                                         // just in the EDT.
        }
        else
            update();
    }
    
    private void update() {
	masterPanel.removeAll();
	if (geneList.size() != 0) {

            /*
             * Global data
             */
            JLabel valTitle = getBoldLabel("Global cluster information:");
            masterPanel.add(valTitle);

            JPanel totalDistancePanel = generateGeneralGenomeInformationPanel("Best total distance: " + cluster.getOccurrences(includeSubOptimalOccurrences).getTotalDist());
            totalDistancePanel.setAlignmentX(Component.LEFT_ALIGNMENT);
            masterPanel.add(totalDistancePanel);

            JPanel bestScorePanel = generateGeneralGenomeInformationPanel("         Best score: " + cluster.getOccurrences(includeSubOptimalOccurrences).getBestScore());
            bestScorePanel.setAlignmentX(Component.LEFT_ALIGNMENT);
            masterPanel.add(bestScorePanel);

            Subsequence refseq = subsequences.get(cluster.getRefSeqIndex());
            JPanel refSeqInterval = generateGeneralGenomeInformationPanel("   Refseq. Interval: [" + refseq.getStart() + "-" + refseq.getStop() + "]");
            refSeqInterval.setAlignmentX(Component.LEFT_ALIGNMENT);
            masterPanel.add(refSeqInterval);

            masterPanel.add(Box.createVerticalStrut(5));

            /*
             * Used parameters
             */
            JLabel parameterLabel = getBoldLabel("Parameters used for computation:");
            masterPanel.add(parameterLabel);
            if (parameters == null)
                masterPanel.add(new JLabel("No Parameters available!"));
            else {
                JPanel parameterPanel = getParameterPanel(parameters);
                parameterPanel.setAlignmentX(LEFT_ALIGNMENT);
                masterPanel.add(parameterPanel);
            }
            masterPanel.add(Box.createVerticalStrut(5));

            /*
             * Local distances to median/center
             */
            JLabel distanceLabel;
            if (cluster.getType() == Parameter.OperationMode.median)
                masterPanel.add(getBoldLabel("Distance to median per dataset:"));
            else if (cluster.getType() == Parameter.OperationMode.center)
                masterPanel.add(getBoldLabel("Distance to center set per dataset:"));
            else
                masterPanel.add(getBoldLabelWithTooltip("Distance to reference gene set per dataset:",
                        "<html>Distance for the gene cluster in each genome to the reference gene set,<br>" + 
                        "which is shown in the panel on the left side.</html>"));
                //masterPanel.add(getBoldLabel("Distance to reference gene set per dataset:"));
            
            masterPanel.add(Box.createVerticalStrut(5));

            JPanel cpanel = generateChromosomeDistancePanel();
            cpanel.setAlignmentX(Component.LEFT_ALIGNMENT);
            masterPanel.add(cpanel);
            masterPanel.add(Box.createVerticalStrut(5));

            /*
             * Local DCJ similarity
             */
            JPanel dcjLabel;
            dcjLabel = getBoldLabelWithTooltip("Local DCJ similarity to reference gene sequence per sequence:",
                    "<html>DCJ similarity for the gene sequence in each genome to the reference gene sequence,<br>" +
                    "which is highlighted with a red background in the panel on the top.</html>");
            
            masterPanel.add(dcjLabel);
            masterPanel.add(Box.createVerticalStrut(5));

            JPanel dpanel = generateDCJSimilarityPanel();
            dpanel.setAlignmentX(Component.LEFT_ALIGNMENT);
            masterPanel.add(dpanel);
            masterPanel.add(Box.createVerticalStrut(5));
            
            /*
             * Reference Genes
             */
            JLabel referenceGenes = getBoldLabel("Genes in the reference gene sequence:");
            masterPanel.add(referenceGenes);
            masterPanel.add(Box.createVerticalStrut(5));
            JPanel referenceGeneTableHeader = new JPanel();
            referenceGeneTableHeader.setLayout(new GridLayout(1, 1));
            referenceGeneTableHeader.setAlignmentX(LEFT_ALIGNMENT);
            referenceGeneTableHeader.add(referenceGeneTable.getTableHeader());
            
            masterPanel.add(referenceGeneTableHeader);
            masterPanel.add(referenceGeneTable);
            masterPanel.add(Box.createVerticalStrut(5));
			
            /*
             * List of genes
             */
            JLabel genesInClusterLabel = getBoldLabel("Genes in this Cluster:");
            masterPanel.add(genesInClusterLabel);
            masterPanel.add(Box.createVerticalStrut(10));
            masterPanel.add(annotationTable);
            masterPanel.add(Box.createVerticalStrut(5));

            /*
             * Empty resizable panel just to fill the masterPanel vertically
             */
            JPanel emptyPanel = new JPanel(new BorderLayout());
            emptyPanel.setBackground(masterPanel.getBackground());
            masterPanel.add(emptyPanel);
            
            /*
	     * Involved Chromosomes
             */
            JLabel invChrTitle = getBoldLabel("Involved chromosomes");
            invChrTitle.setAlignmentX(Component.LEFT_ALIGNMENT);
            masterPanel.add(invChrTitle);

            masterPanel.add(Box.createVerticalStrut(5));

            masterPanel.add(chromosomeNameTable);            
	}
	this.repaint();
	this.revalidate();
	this.getVerticalScrollBar().setValue(0);
        this.getHorizontalScrollBar().setValue(0);
    }

    private static JLabel getBoldLabel(String text) {
        JLabel label = new JLabel(text);
        label.setFont(boldFont);
        return label;
    }
    
    public static JPanel getBoldLabelWithTooltip(String text, String tooltipText) {
        JLabel label = getBoldLabel(text);
        
        JLabel tooltip = new JLabel("?");
        tooltip.setFont(tooltip.getFont().deriveFont(10f));
        tooltip.setForeground(Color.GRAY);
        
        JPanel tooltipPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 0));
        tooltipPanel.setBackground(Color.WHITE);
        tooltipPanel.setBorder(BorderFactory.createLineBorder(Color.GRAY, 1));
        tooltipPanel.setPreferredSize(new Dimension(tooltip.getPreferredSize().height, tooltip.getPreferredSize().height));
        tooltipPanel.add(tooltip);
        tooltipPanel.setToolTipText(tooltipText);

        JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEADING, 0, 0));
        panel.setBackground(Color.WHITE);
        panel.setAlignmentX(LEFT_ALIGNMENT);
        
        panel.add(label);
        panel.add(Box.createRigidArea(new Dimension(5,0)));
        panel.add(tooltipPanel);
        
        return panel;
    }

    private static JLabel getMonoLabel(String text) {
        JLabel label = new JLabel(text);
        label.setFont(monoFont);
        return label;
    }
    
    private void appendParameterLabel(JPanel panel, String label) {
        panel.add(getMonoLabel(label));
        appendParameterSpacing(panel);
    }
    
    private void appendParameterSpacing(JPanel panel) {
        panel.add(Box.createRigidArea(new Dimension(0,3)));
    }

    private JPanel getParameterPanel(Parameter parameters) {
        // New layout without jgoodies, not this part doesn't resize
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
        panel.setAlignmentX(LEFT_ALIGNMENT);
        panel.setBackground(masterPanel.getBackground());
        appendParameterSpacing(panel);

        if (parameters.useDeltaTable()) {
            appendParameterLabel(panel, "Reference mode: " + parameters.getRefType());
            if (parameters.searchRefInRef())
                appendParameterLabel(panel, "Ref. in Ref.");
            appendParameterLabel(panel, "Quorum: " + parameters.getQ());
            appendParameterLabel(panel, "Distance (\u03B4) Table:");
            
            JTable deltaTable = new JTable(new DeltaTableModel(parameters.getDeltaTable(), parameters.getMinClusterSize()));
            JPanel deltaTablePanel = new JPanel();
            deltaTablePanel.setLayout(new BoxLayout(deltaTablePanel, BoxLayout.PAGE_AXIS));
            deltaTablePanel.setAlignmentX(LEFT_ALIGNMENT);
            deltaTablePanel.add(deltaTable.getTableHeader());
            deltaTablePanel.add(deltaTable);
            panel.add(deltaTablePanel);
            
            appendParameterSpacing(panel);
        } else {
            appendParameterLabel(panel, "Reference mode: " + parameters.getRefType());
            if (parameters.searchRefInRef())
                appendParameterLabel(panel, "Ref. in Ref.");
            appendParameterLabel(panel, "Max. Distance (\u03B4): " + parameters.getDelta());
            appendParameterLabel(panel, "<html>Min. Size (<i>s</i>): </html>" + parameters.getMinClusterSize());
            appendParameterLabel(panel, "Quorum: " + parameters.getQ());
        }
        
        appendParameterLabel(panel, "Local DCJ similarity parameters:");
        appendParameterLabel(panel, "  Discarded genes penality: " + parameters.getDiscardedPenality());
        appendParameterLabel(panel, "  Borderline length: " + parameters.getBorderlineLength());
        if (parameters.computeSubintervals()) {
            appendParameterLabel(panel, "  Similarity for subintervals computed");
            if (parameters.getExtendSubsequences() > 0)
                appendParameterLabel(panel, "  Subsequences extended by: " + parameters.getExtendSubsequences() + " gene(s)");
        }
        if (parameters.useHeuristics())
            appendParameterLabel(panel, "  Compute using heuristics");
        
        return panel;
    }

    private JPanel generateGeneralGenomeInformationPanel(String text) {
        JPanel cpanel = new JPanel();
        FlowLayout f = new FlowLayout(FlowLayout.LEFT);
        f.setVgap(1);
        cpanel.setLayout(f);
        cpanel.setBackground(masterPanel.getBackground());
        JLabel textLabel = getMonoLabel(text);
        cpanel.add(textLabel);
        return cpanel;
    }

    private JPanel generateChromosomeDistancePanel() {  //TODO smarter Object?
        JPanel cpanel = new JPanel();
        FlowLayout f = new FlowLayout(FlowLayout.LEFT);
        f.setVgap(1);
        cpanel.setLayout(f);
        cpanel.setBackground(masterPanel.getBackground());

        for (int i=0; i<subsequences.size(); i++) {
            cpanel.add(new NumberInRectangle(genomeIndexMapping.get(i)+1, getBackground(), chromosomes.get(i).getChromosomeMouseListener()));
            JLabel textLabel = getMonoLabel(Integer.toString(subsequences.get(i).getDist()));
            cpanel.add(textLabel);
            cpanel.add(Box.createHorizontalStrut(5));
        }
        return cpanel;
    }

    private JPanel generateDCJSimilarityPanel() {
        JPanel panel = new JPanel();
        FlowLayout f = new FlowLayout(FlowLayout.LEFT, 0, 0);
        panel.setLayout(f);
        panel.setBackground(masterPanel.getBackground());
        
        int refseqindex = cluster.getRefSeqIndex();
        
        for (int i=0; i<subsequences.size(); i++) {
            DCJSimilarityPanel simPanel = new DCJSimilarityPanel(genomeIndexMapping.get(i)+1,
                                                                 subsequences.get(i),
                                                                 subsequences.get(refseqindex),
                                                                 chromosomes.get(i),
                                                                 chromosomes.get(refseqindex),
                                                                 monoFont,
                                                                 masterPanel.getBackground(),
                                                                 parameters);
            
            panel.add(simPanel);
            panel.add(Box.createHorizontalStrut(5));
        }
        return panel;
    }


    private void setGeneData() {
        Map<GeneFamily, Gene[]> annotations = cluster.generateAnnotations(includeSubOptimalOccurrences,
                subselections);
        setMaxLengthStringWidth(GeneCluster.getMaximumIdLength(annotations));

        this.setAnnotationTable(annotations);
        this.setReferenceTable(annotations);
    }

    private void setReferenceTable(Map<GeneFamily, Gene[]> annotations) {
        final TableColumnModel tableColumnModel = referenceGeneTable.getColumnModel();
        tableColumnModel.getColumn(0).setPreferredWidth(geneWidth);
        tableColumnModel.getColumn(0).setMaxWidth(geneWidth);

        List<Gene> genes = cluster.getGenes(includeSubOptimalOccurrences, cluster.getRefSeqIndex(), subselections[cluster.getRefSeqIndex()]);
        for (Gene gene : genes) {
            ReferenceGeneInfo info;
            if (gene.getGeneFamily().isSingleGeneFamily()){
                info = new ReferenceGeneInfo(gene.getGeneFamily(), 1, gene);
            } else {
                Gene[] geneArray = annotations.get(gene.getGeneFamily());
                int occsInGenomes = 0;
                if (geneArray != null) {
                    for (int i=0;i<geneArray.length;i++) {
                        if (geneArray[i] != null)
                            occsInGenomes++;
                    }
                } else {
                    occsInGenomes = 1;
                }
                info = new ReferenceGeneInfo(gene.getGeneFamily(), occsInGenomes, gene);
            }
            referenceGeneList.add(info);
        }
    }
    
    public static class ReferenceGeneInfo{
        public final GeneFamily geneFamily;
        public final int occsInGenomes;
        public final Gene refGene;

        private ReferenceGeneInfo(GeneFamily geneFamily, int occsInGenomes, Gene refGene) {
            this.geneFamily = geneFamily;
            this.occsInGenomes = occsInGenomes;
            this.refGene = refGene;
        }
    }

    private void setAnnotationTable(Map<GeneFamily, Gene[]> annotations) {
        final TableColumnModel annotationTableColumnModel = annotationTable.getColumnModel();
        annotationTableColumnModel.getColumn(0).setPreferredWidth(geneWidth);
        annotationTableColumnModel.getColumn(0).setMaxWidth(geneWidth);

        for (Map.Entry<GeneFamily, Gene[]> entry : annotations.entrySet()) {
            geneIdAtTablePosition.put(geneList.size(), entry.getKey());
            Gene[] genes = entry.getValue();

            for (int i=0;i<genes.length;i++) {
                if (genes[i] != null) {
                    geneList.add(genes[i]);
                    genomeIndexInGeneList.add(i);
                }
            }
        }
    }

    private class ChromosomeNameTableModel extends AbstractTableModel {
        private static final long serialVersionUID = -3306238610287868813L;

        private final Class<?>[] columns = {NumberInRectangle.NumberIcon.class, String.class};

        @Override
        public int getRowCount() {
            return chromosomes.size();
        }

        @Override
        public int getColumnCount() {
            return columns.length;
        }

        @Override
        public Class<?> getColumnClass(int columnIndex)	{
            return this.columns[columnIndex];
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            switch (columnIndex) {
                case 0:
                    return new NumberInRectangle.NumberIcon(genomeIndexMapping.get(rowIndex)+1);
                case 1:
                    Subsequence seq = subsequences.get(rowIndex);
                    return chromosomes.get(rowIndex).getFullName() + " (interval [" + seq.getStart() + "-" + seq.getStop() + "])";
                default:
                    return null;
            }
        }
    }

    private class ReferenceGeneTableModel extends AbstractTableModel {
        private final Class<?>[] columns = {GeneFamily.class, Integer.class, String.class};
        private final String[] tableHeaders = {"Gene", "#Genomes", "Annotation"};

        @Override
        public int getRowCount() {
            return referenceGeneList.size();
        }

        @Override
        public int getColumnCount() {
            return columns.length;
        }

        @Override
        public Class<?> getColumnClass(int columnIndex)	{
            return this.columns[columnIndex];
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            switch (columnIndex) {
                case 0:
                    return referenceGeneList.get(rowIndex).geneFamily;
                case 1:
                    return referenceGeneList.get(rowIndex).occsInGenomes;
                case 2:
                    return referenceGeneList.get(rowIndex).refGene.getSummary();
                default:
                    return null;
            }
        }

        @Override
        public String getColumnName(int columnIndex) {
            return tableHeaders[columnIndex];
        }
    }

    private class GeneAnnotationTableModel extends AbstractTableModel {
        private static final long serialVersionUID = -3306238610287868813L;

        private final Class<?>[] columns = {GeneFamily.class, NumberInRectangle.NumberIcon.class, String.class};

        @Override
        public int getRowCount() {
            return geneList.size();
        }

        @Override
        public int getColumnCount() {
            return columns.length;
        }

        @Override
        public Class<?> getColumnClass(int columnIndex)	{
            return this.columns[columnIndex];
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            switch (columnIndex) {
                case 0:
                    return geneIdAtTablePosition.get(rowIndex);
                case 1:
                    return new NumberInRectangle.NumberIcon(genomeIndexInGeneList.get(rowIndex)+1);
                case 2:
                    return geneList.get(rowIndex).getSummary();
                default:
                    return null;
            }
        }
    }

    private class DeltaTableModel extends AbstractTableModel {
        private final Class<?>[] columns = {Integer.class, Integer.class, Integer.class, Integer.class};
        private final String[] columnTitles = {"<html>\u03B4<sup>add</sup></html>","<html>\u03B4<sup>loss</sup></html>", "<html>\u03B4<sup>sum</sup></html>", "Size"};

        List<int[]> deltaValues;

        public DeltaTableModel(int[][] values, int minSize) {
            super();
            deltaValues = new ArrayList<>();
            for (int i=minSize; i<values.length; i++) {
                if (values[i][0] != -1){
                    deltaValues.add(new int[]{values[i][0], values[i][1], values[i][2], i});
                }
            }
        }

        @Override
        public boolean isCellEditable(int rowIndex, int columnIndex) {
            return false;
        }

        @Override
        public int getRowCount() {
            return deltaValues.size();
        }

        @Override
        public int getColumnCount() {
            return columns.length;
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            return deltaValues.get(rowIndex)[columnIndex];
        }

        @Override
        public String getColumnName(int columnIndex) {
            return columnTitles[columnIndex];
        }
    }

    private class NumberIconRenderer extends DefaultTableCellRenderer.UIResource {
        public NumberIconRenderer() {
            super();
            setHorizontalAlignment(JLabel.LEFT);
        }

        @Override
        public void setValue(Object value) {
            if (value instanceof NumberInRectangle.NumberIcon) {
                NumberInRectangle.NumberIcon numberIcon = (NumberInRectangle.NumberIcon)value;
                setIcon(numberIcon);
                setToolTipText(chromosomes.get(genomeIndexBackmap.get(numberIcon.getNumber() - 1)).getFullName());
            } else
                setIcon(null);
        }
    }

    private class GeneRenderer extends DefaultTableCellRenderer.UIResource {
        public GeneRenderer() {
            super();
            setHorizontalAlignment(JLabel.LEFT);
        }

        @Override
        public void setValue(Object value) {
            if (value instanceof GeneFamily) {
                GenomePainting.GeneIcon icon = new GenomePainting.GeneIcon((GeneFamily)value, geneWidth, MultipleGenomesBrowser.DEFAULT_GENE_HEIGHT);
                setIcon(icon);
            } else
                setIcon(null);
        }
    }
}
