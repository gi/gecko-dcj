/*
 * Copyright 2019 Diego Rubert
 * Copyright 2014 Sascha Winter, Tobias Mann, Hans-Martin Haase, Leon Kuchenbecker and Katharina Jahn
 *
 * This file is part of Gecko3.
 *
 * Gecko3 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gecko3 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Gecko3.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.unijena.bioinf.gecko3.gui;

import com.jgoodies.forms.builder.ButtonBarBuilder;
import com.jgoodies.forms.builder.DefaultFormBuilder;
import com.jgoodies.forms.factories.Borders;
import com.jgoodies.forms.layout.FormLayout;
import de.unijena.bioinf.gecko3.GeckoInstance;
import de.unijena.bioinf.gecko3.GeckoInstance.ResultFilter;
import de.unijena.bioinf.gecko3.gui.util.DisabledGlassPane;
import de.unijena.bioinf.gecko3.io.ExportType;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Frame;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class ResultExportDialog extends JDialog {

    /**
     * Random generated serial version uid
     */
    private static final long serialVersionUID = -5557686856082270849L;

    private final JTextField textField;
    private final JPanel advancedOptionsPanel;

    /**
     * Create the dialog.
     */
    public ResultExportDialog(Frame parent) {
        super(parent);
        setTitle("Export Results");
        setIconImages(parent.getIconImages());

        DefaultFormBuilder generalOptionsBuilder = new DefaultFormBuilder(new FormLayout("p, 4dlu, p:g, 4dlu, p"));
        generalOptionsBuilder.border(Borders.DIALOG);

        textField = new JTextField();
        JButton btnBrowse = new JButton("Browse...");
        btnBrowse.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                GeckoInstance gecko = GeckoInstance.getInstance();
                JFileChooser fc = new JFileChooser(gecko.getCurrentWorkingDirectoryOrFile());
                int state = fc.showSaveDialog(ResultExportDialog.this);
                if (state == JFileChooser.APPROVE_OPTION) {
                    if (fc.getSelectedFile() != null) {
                        textField.setText(fc.getSelectedFile().getAbsolutePath());
                        gecko.setCurrentWorkingDirectoryOrFile(fc.getSelectedFile());
                    }
                }
            }
        });
        final JComboBox<ExportType> exportTypeCompoBox = new JComboBox<>(ExportType.getSupported());
        exportTypeCompoBox.setEditable(false);
        exportTypeCompoBox.setMaximumRowCount(ExportType.getSupported().length);
        exportTypeCompoBox.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if (e.getStateChange() == ItemEvent.SELECTED) {
                    CardLayout cl = (CardLayout) (advancedOptionsPanel.getLayout());
                    cl.show(advancedOptionsPanel, e.getItem().toString());
                }
            }
        });
        
        List<String> tooltips = new ArrayList<>(16);
        for (String line : ExportType.types.split("\n")) {
            String s = line.replaceFirst("\"[A-Za-z0-9_ ]*\" ", "");
            s = s.substring(0, 1).toUpperCase() + s.substring(1);
            tooltips.add(s);
        }
        exportTypeCompoBox.setRenderer(new ComboboxToolTipRenderer(tooltips));

        final JComboBox<ResultFilter> resultFilterComboBox = new JComboBox<>(ResultFilter.values());
        resultFilterComboBox.setEditable(false);

        generalOptionsBuilder.append(new JLabel("Choose Export Type"), exportTypeCompoBox);
        generalOptionsBuilder.nextLine();
        generalOptionsBuilder.append(new JLabel("Choose Filtering"), resultFilterComboBox);
        generalOptionsBuilder.nextLine();
        generalOptionsBuilder.append(new JLabel("Choose File"), textField, btnBrowse);

        advancedOptionsPanel = new JPanel(new CardLayout());
        for (ExportType exportType : ExportType.getSupported()) {
            advancedOptionsPanel.add(exportType.getAdditionalOptionsPanel(), exportType.toString());
        }

        ButtonBarBuilder buttonBuilder = new ButtonBarBuilder();

        JButton okButton = new JButton("OK");
        okButton.setActionCommand("OK");
        okButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                final File file = validateSettings();
                if (file != null) {
                    ExportType type = (ExportType) exportTypeCompoBox.getSelectedItem();
                                        
                    if (type.getAdditionalExportParameters() != null && type.getAdditionalExportParameters().getIncludeDCJ()) { // different case, we use a GUI and this goes to a thread                   
                        final int defaultCloseOperation = ResultExportDialog.this.getDefaultCloseOperation();
                        final DisabledGlassPane disabledGlassPane = new DisabledGlassPane();
                        ResultExportDialog.this.setGlassPane(disabledGlassPane);
                        disabledGlassPane.activate();
                        ResultExportDialog.this.setEnabled(false);
                        
                        ResultExportDialog.this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
                        new Thread(new Runnable() { 
                            @Override
                            public void run() 
                            { 
                                if (!GeckoInstance.getInstance().exportResultsToFile(file, (ResultFilter) resultFilterComboBox.getSelectedItem(), (ExportType) exportTypeCompoBox.getSelectedItem())) {
                                    JOptionPane.showMessageDialog(ResultExportDialog.this, "Error writing the file!", "Write error", JOptionPane.ERROR_MESSAGE);
                                }
                                disabledGlassPane.deactivate();
                                ResultExportDialog.this.setDefaultCloseOperation(defaultCloseOperation);
                                ResultExportDialog.this.close();
                            } 
                        }).start();
                    }
                    else {
                        if (!GeckoInstance.getInstance().exportResultsToFile(file, (ResultFilter) resultFilterComboBox.getSelectedItem(), (ExportType) exportTypeCompoBox.getSelectedItem())) {
                                JOptionPane.showMessageDialog(ResultExportDialog.this, "Error writing the file!", "Write error", JOptionPane.ERROR_MESSAGE);
                            }
                        ResultExportDialog.this.close();
                    }
                    
                }
            }
        });

        JButton cancelButton = new JButton("Cancel");
        cancelButton.setActionCommand("Cancel");
        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ResultExportDialog.this.close();
            }
        });

        buttonBuilder.addGlue();
        buttonBuilder.addButton(okButton);
        buttonBuilder.addRelatedGap();
        buttonBuilder.addButton(cancelButton);

        JPanel generalOptionPanel = generalOptionsBuilder.getPanel();

        DefaultFormBuilder contentBuilder = new DefaultFormBuilder(new FormLayout("p"));
        contentBuilder.append(generalOptionPanel);
        contentBuilder.nextLine();
        contentBuilder.appendSeparator("Additional Options");
        contentBuilder.append(advancedOptionsPanel);
        JPanel contentPanel = contentBuilder.getPanel();

        JPanel buttonPanel = buttonBuilder.getPanel();
        buttonPanel.setBorder(Borders.DIALOG);

        getContentPane().setLayout(new BorderLayout());
        getContentPane().add(contentPanel, BorderLayout.CENTER);
        getContentPane().add(buttonPanel, BorderLayout.SOUTH);
        pack();
        
        // BUG FIX:
        // The text field resizes and goes beyond the border of the window
        // when we select a file and then change the export type
        textField.setPreferredSize(new Dimension(textField.getPreferredSize().width, textField.getPreferredSize().height));

        // BUG FIX:
        // It seems that by some unkown circunstance, the comboboxes of
        // this window don't work properly. We have to resize the window
        // and then they'll work as expected. After this, we revert the
        // window to the original size and center it in the screen
        int w = getPreferredSize().width;
        int h = getPreferredSize().height;
        setSize(new Dimension(w + 1, h + 1));
        setSize(new Dimension(w, h));
        pack();
        setLocationRelativeTo(null);        
    }

    /**
     * Validate all settings to check if a result can be exported
     *
     * @return the File the results will be written to, or null if not valid
     */
    private File validateSettings() {
        if (textField.getText().trim().equals("")) {
            JOptionPane.showMessageDialog(ResultExportDialog.this, "You have to select a file!");
            return null;
        }
        File file = new File(textField.getText());
        if (file.exists()) {
            int x = JOptionPane.showConfirmDialog(ResultExportDialog.this,
                    "The chosen file already exists. Overwrite?",
                    "Overwrite existing file?",
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.WARNING_MESSAGE);
            if (x == JOptionPane.NO_OPTION) {
                return null;
            }
        }

        return file;
    }

    /**
     * Close the dialog
     */
    private void close() {
        this.setVisible(false);
    }

    private class ComboboxToolTipRenderer extends DefaultListCellRenderer {

        List<String> tooltips;

        public ComboboxToolTipRenderer(List<String> tooltips) {
            super();
            this.tooltips = tooltips;
        }
        
        @Override
        public Component getListCellRendererComponent(JList list, Object value,
                int index, boolean isSelected, boolean cellHasFocus) {

            JComponent comp = (JComponent) super.getListCellRendererComponent(list,
                    value, index, isSelected, cellHasFocus);

            if (-1 < index && null != value && null != tooltips) {
                if (index < tooltips.size())
                    list.setToolTipText(tooltips.get(index));
                else
                    list.setToolTipText(null);
            }
            return comp;
        }

        public void setTooltips(List<String> tooltips) {
            this.tooltips = tooltips;
        }
    }
}
