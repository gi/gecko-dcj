/*
 * Copyright 2019 Diego Rubert
 *
 * This file is part of Gecko3.
 *
 * Gecko3 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gecko3 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Gecko3.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Window showing an Adjacency Graph
 * @author Diego Rubert
 */


package de.unijena.bioinf.gecko3.gui;

import de.unijena.bioinf.gecko3.datastructures.adjacencygraph.AdjacencyGraph;
import java.awt.Dimension;
import java.awt.HeadlessException;
import javax.swing.ImageIcon;
import javax.swing.JFrame;


public class AdjacencyGraphWindow extends JFrame {

    public AdjacencyGraphWindow(AdjacencyGraph ag) throws HeadlessException {
        super("Adjacency Graph: " + ag.getLabel());
        
        //this.setContentPane(agPanel);
        this.setResizable(false);
        this.setIconImage(new ImageIcon(ClassLoader.getSystemResource("images/graph.png")).getImage());
        this.setPreferredSize(new Dimension(100, 100));
        this.pack();
        //this.setLocationRelativeTo(null);
    }
    
    
}

/*
        MutableGraph g = mutGraph("example1").setDirected(true).add(
        mutNode("a").add(Color.RED).addLink(mutNode("b")));
Graphviz.fromGraph(g).width(200).render(Format.PNG).toFile(new File("example/ex1m.png"));
*/