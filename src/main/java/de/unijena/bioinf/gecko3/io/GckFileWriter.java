/*
 * Copyright 2019 Diego Rubert
 * Copyright 2014 Sascha Winter, Tobias Mann, Hans-Martin Haase, Leon Kuchenbecker and Katharina Jahn
 *
 * This file is part of Gecko3.
 *
 * Gecko3 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gecko3 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Gecko3.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.unijena.bioinf.gecko3.io;

import de.unijena.bioinf.gecko3.datastructures.*;
import de.unijena.bioinf.gecko3.datastructures.util.DCJSimilarityData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.AbstractMap;
import java.util.Arrays;
import java.util.List;
import java.util.zip.GZIPOutputStream;

/**
 * The class implements a writer which writes a gecko session (.gck) or a gecko
 * compressed session (.gckz) to a file.
 */
public class GckFileWriter {

    private static final Logger logger = LoggerFactory.getLogger(GckFileWriter.class);

    final static String SEPERATOR = "\t";

    final static String GENOME_SECTION_START = "<genomes>";
    final static String GENOME_SECTION_END = "</genomes>";
    final static String GENOME_START = "<genome>";
    final static String GENOME_END = "</genome>";
    final static String CHROMOSOME_START = "<chromosome>";
    final static String CHROMOSOME_END = "</chromosome>";

    final static String CLUSTER_SECTION_START = "<clusters>";
    final static String CLUSTER_SECTION_END = "</clusters>";
    final static String PARAMETERS_START = "<parameters>";
    final static String PARAMETERS_END = "</parameters>";
    final static String CLUSTER_START = "<cluster>";
    final static String CLUSTER_END = "</cluster>";
    final static String OCC_START = "<occ>";
    final static String OCC_END = "</occ>";
    final static String DCJ_START = "<dcj>";
    final static String DCJ_END = "</dcj>";

    /**
     * Saves the current gecko session to a given file
     *
     * @param f The file to write to
     */
    public static boolean saveDataSetToFile(DataSet data, File f) {
        return saveDataSetToFile(data, f, false);
    }
    
    public static boolean saveDataSetToFile(DataSet data, File f, boolean compressed) {
        boolean returnValue = true;

        try (
            BufferedWriter out = openFile(f, compressed);
        ) {
            writeGenomes(out, data.getGenomes());
            writeClusters(out, data);
        } catch (IOException e) {
            logger.warn("Unable to write dataset", e);
            returnValue = false;
        }
        return returnValue;
    }
    
    private static BufferedWriter openFile(File f, boolean compressed) throws IOException {
        if (compressed) {
            GZIPOutputStream gzip = new GZIPOutputStream(new FileOutputStream(f));
            return new BufferedWriter(new OutputStreamWriter(gzip, Charset.forName("UTF-8")));

        } else
            return Files.newBufferedWriter(f.toPath(), Charset.forName("UTF-8"));
    }

    private static void writeGenomes(BufferedWriter out, Genome[] genomes) throws IOException {
        out.write(GENOME_SECTION_START);
        out.newLine();
        for (Genome genome : genomes) {
            out.write(GENOME_START);
            out.newLine();
            out.write(genome.getName());
            out.newLine();
            for (Chromosome chr : genome.getChromosomes()) {
                out.write(CHROMOSOME_START);
                out.newLine();
                out.write(chr.getName());
                out.newLine();
                for (Gene gene : chr.getGenes()) {
                    out.write(gene.getOrientation().getEncoding() + SEPERATOR + gene.getExternalId() + SEPERATOR + gene.getTag() + SEPERATOR + gene.getAnnotation() + SEPERATOR + gene.getName() + SEPERATOR + (gene.isUnknown() ? 1 : gene.getFamilySize()));
                    out.newLine();
                }
                out.write(CHROMOSOME_END);
                out.newLine();
            }
            out.write(GENOME_END);
            out.newLine();
        }
        out.write(GENOME_SECTION_END);
        out.newLine();
    }

    private static void writeClusters(BufferedWriter out, DataSet data) throws IOException {
        List<GeneCluster> clusters = data.getClusters();
        Parameter parameters = data.getParameters();
        Genome[] genomes = data.getGenomes();

        out.write(CLUSTER_SECTION_START);
        out.newLine();
        if (parameters != null) {
            out.write(PARAMETERS_START);
            out.newLine();
            writeParameters(out, parameters);
            out.newLine();
            out.write(PARAMETERS_END);
            out.newLine();
        }
        for (GeneCluster cluster : clusters) {
            GeneClusterOccurrence occurrences = cluster.getOccurrences(true);
            Subsequence[][] subsequences = occurrences.getSubsequences();
            int refSeqIndex = cluster.getRefSeqIndex();

            out.write(CLUSTER_START);
            out.newLine();
            out.write(cluster.getId() + SEPERATOR + cluster.getRefSeqIndex() + SEPERATOR + cluster.getType().getCharMode() + SEPERATOR + cluster.getMinTotalDist() + SEPERATOR + cluster.getBestPValue() + SEPERATOR + cluster.getBestPValueCorrected() + SEPERATOR + cluster.getAvgDCJSim());
            out.newLine();
            out.write(cluster.getGeneFamilyString());
            out.newLine();

            // Writes OCCs
            out.write(OCC_START);
            out.newLine();
            out.write(occurrences.getId() + SEPERATOR + occurrences.getBestpValue() + SEPERATOR + occurrences.getSupport() + SEPERATOR + occurrences.getTotalDist());
            out.newLine();
            for (int i = 0; i < subsequences.length; i++) {
                for (Subsequence sub : subsequences[i]) {
                    out.write(i + SEPERATOR + sub.getChromosome() + SEPERATOR + sub.getDist() + SEPERATOR + sub.getStart() + SEPERATOR + sub.getStop() + SEPERATOR + sub.getpValue());
                    out.newLine();

                    // For the current subseq, writes DCJ similarities, one to each reference gene sequence
                    List<AbstractMap.SimpleImmutableEntry<Subsequence, DCJSimilarityData>> similarityEntries = sub.getDCJSimList();
                    if (similarityEntries.isEmpty())
                        continue;
                    out.write(DCJ_START);
                    out.newLine();

                    // i is the genome number
                    // subsequences[i] is the list of subsequences in the genome i
                    // sub.getChromosome() is the chromosome number in the genome i
                    // refSeqIndex is the genome number of the reference sequence
                    // refseq.getChromosome() is the chromosome number of the reference sequence in the genome number refSeqIndex
                    for (AbstractMap.SimpleImmutableEntry<Subsequence, DCJSimilarityData> entry : similarityEntries) {
                        Subsequence refseq = entry.getKey();
                        DCJSimilarityData dcjData = entry.getValue();
                        out.write(refSeqIndex + SEPERATOR + refseq.getChromosome() + SEPERATOR + refseq.getStart() + SEPERATOR + refseq.getStop());
                        out.write(SEPERATOR + dcjData.sim() + SEPERATOR + dcjData.discarded() + SEPERATOR + dcjData.cycles() + SEPERATOR + dcjData.odd() + SEPERATOR + dcjData.even());
                        out.write(SEPERATOR + dcjData.startA() + SEPERATOR + dcjData.stopA() + SEPERATOR + dcjData.startB() + SEPERATOR + dcjData.stopB());
                        short[] map = dcjData.genesMap();
                        if (map != null && map.length > 0) {
                            out.write(SEPERATOR + map[0]);
                            for (int idx = 1; idx < map.length; ++idx)
                                out.write("," + map[idx]);
                        }
                        out.newLine();
                    }
                    out.write(DCJ_END);
                    out.newLine();

                }
            }

            out.write(OCC_END);
            out.newLine();
            out.write(CLUSTER_END);
            out.newLine();
        }
        out.write(CLUSTER_SECTION_END);
        out.newLine();
    }

    /**
     * Writes the parameters. parameters must not be null!
     *
     * @param out
     * @param p must not be null
     * @throws IOException
     */
    private static void writeParameters(BufferedWriter out, Parameter p) throws IOException {
        out.write(p.getOperationModeChar() + SEPERATOR + p.getRefTypeChar() + SEPERATOR + (p.searchRefInRef() ? 1 : 0) + SEPERATOR + p.getMinClusterSize() + SEPERATOR + p.getQ() + SEPERATOR);
        if (p.useDeltaTable()) {
            out.write(Arrays.deepToString(p.getDeltaTable()));
        } else {
            out.write(Integer.toString(p.getDelta()));
        }
        out.write(SEPERATOR + p.getDiscardedPenality() + SEPERATOR + p.getBorderlineLength() + SEPERATOR + (p.computeSubintervals() ? 1 : 0) + SEPERATOR + p.getExtendSubsequences() + SEPERATOR + (p.preCompute() ? 1 : 0) + SEPERATOR + (p.useHeuristics()? 1 : 0));
    }
}
