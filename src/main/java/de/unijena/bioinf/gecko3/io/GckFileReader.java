/*
 * Copyright 2019 Diego Rubert
 * Copyright 2014 Sascha Winter, Tobias Mann, Hans-Martin Haase, Leon Kuchenbecker and Katharina Jahn
 *
 * This file is part of Gecko3.
 *
 * Gecko3 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Gecko3 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Gecko3.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.unijena.bioinf.gecko3.io;

import de.unijena.bioinf.gecko3.datastructures.*;
import de.unijena.bioinf.gecko3.datastructures.util.DCJSimilarityData;
import de.unijena.bioinf.gecko3.io.util.StringParsing;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.text.ParseException;
import java.util.*;
import java.util.zip.GZIPInputStream;

/**
 * The class implements a reader for .gck files (session files) or .gckz
 * (compressed session files). The code is exported from GeckoInstance.java
 * and modified.
 *
 * @author Hans-Martin Haase
 * @version 0.03
 */
public class GckFileReader implements GeckoDataReader {

    GeneFamilySet geneFamilies;

    /**
     * Storing place for the genomes.
     */
    private Genome[] genomes;

    /**
     * Storing place for the gene clusters.
     */
    private List<GeneCluster> clusters;
    private Parameter parameters;

    /**
     * Storing place for the length of the longest id.
     */
    private int maxIdLength;

    /**
     * Storing place for the length of the longest name.
     */
    private int maxNameLength;

    /**
     * Storing place for the length of the longest locus tag.
     */
    private int maxLocusTagLength;

    /**
     * The input file
     */
    private final File inputFile;
    
    /*
     * If the input file is compressed
     */
    private final boolean compressed;

    
    public GckFileReader(File gckInputFile) {
        inputFile = gckInputFile;
        this.compressed = false;
    }
    
    public GckFileReader(File gckInputFile, boolean compressed) {
        inputFile = gckInputFile;
        this.compressed = compressed;
    }

    /**
     * Reads all data from the file
     */
    @Override
    public DataSet readData() throws IOException, ParseException {
        try (BufferedReader reader = openFile()) {
            geneFamilies = new GeneFamilySet();

            String line = reader.readLine().trim();
            if (!line.equals(GckFileWriter.GENOME_SECTION_START)) {
                throw new ParseException("Malformed first line: " + line, 0);
            }
            readGenomeData(reader, geneFamilies);
            line = reader.readLine().trim();
            if (!line.startsWith(GckFileWriter.CLUSTER_SECTION_START)) {
                throw new ParseException("Malformed cluster section start: " + line, 0);
            }
            readClusterData(reader, geneFamilies);
        } catch (IOException | ParseException e) {
            handleFailedSessionLoad();
            throw e;
        }
        return new DataSet(
                genomes,
                maxIdLength,
                maxNameLength,
                maxLocusTagLength,
                geneFamilies,
                clusters,
                parameters
        );
    }
    
    private BufferedReader openFile() throws IOException {
        if (compressed) {
            GZIPInputStream gzip = new GZIPInputStream(new FileInputStream(inputFile));
            return new BufferedReader(new InputStreamReader(gzip, Charset.forName("UTF-8")));

        } else
            return Files.newBufferedReader(inputFile.toPath(), Charset.forName("UTF-8"));
    }
    
    private static short[] parseStrToShortArray(String str) {
        String[] strArr = str.split(",");
        short[] shortArr = new short[strArr.length];
        for (int i = 0; i < strArr.length; i++)
            shortArr[i] = Short.parseShort(strArr[i]);
        return shortArr;
    }

    private void readGenomeData(BufferedReader reader, GeneFamilySet geneFamilies) throws IOException, ParseException {
        List<Genome> genomeList = new ArrayList<>();
        Genome genome = null;
        boolean continueReading = true;
        while (continueReading) {
            String line = reader.readLine().trim();
            switch (line) {
                case GckFileWriter.GENOME_START:
                    if (genome != null) {
                        throw new ParseException("Genome or chromosome not closed before new genome start.", 0);
                    }
                    genome = new Genome(reader.readLine().trim());
                    genomeList.add(genome);
                    break;
                case GckFileWriter.GENOME_END:
                    genome = null;
                    break;
                case GckFileWriter.CHROMOSOME_START:
                    if (genome == null) {
                        throw new ParseException("Not in Genome at chromosome start!", 0);
                    }
                    genome.addChromosome(readChromosome(reader, genome, geneFamilies));
                    break;
                case GckFileWriter.GENOME_SECTION_END:
                    if (genome != null) {
                        throw new ParseException("Genome or chromosome not closed at genomes end.", 0);
                    }
                    continueReading = false;
                    break;
                default:
                    throw new ParseException("Maleformed line: " + line, 0);
            }
        }
        genomes = genomeList.toArray(new Genome[genomeList.size()]);
    }

    private Chromosome readChromosome(BufferedReader reader, Genome genome, GeneFamilySet geneFamilies) throws IOException, ParseException {
        Chromosome chr = new Chromosome(reader.readLine().trim(), genome);
        chr.setGenes(new ArrayList<Gene>());

        boolean continueReading = true;
        while (continueReading) {
            String line = reader.readLine().trim();
            if (line.equals(GckFileWriter.CHROMOSOME_END)) {
                continueReading = false;
            } else {
                String[] split = line.split(GckFileWriter.SEPERATOR);

                GeneFamily geneFamily = geneFamilies.addGene(split[1]);

                Gene.GeneOrientation orientation;
                switch (split[0]) {
                    case "+":
                        orientation = Gene.GeneOrientation.POSITIVE;
                        break;
                    case "-":
                        orientation = Gene.GeneOrientation.NEGATIVE;
                        break;
                    default:
                        orientation = Gene.GeneOrientation.UNSIGNED;
                }

                String annotation = split[3].trim();
                if (annotation.equals("null"))
                    annotation = null;
                Gene newGene = new Gene(split[4].trim(), split[2].trim(), geneFamily, orientation, annotation);
                maxIdLength = Math.max(maxIdLength, (split[1].length()));
                maxLocusTagLength = Math.max(maxLocusTagLength, newGene.getTag().length());
                maxNameLength = Math.max(maxNameLength, newGene.getName().length());

                chr.getGenes().add(newGene);
            }
        }
        return chr;
    }

    private void readClusterData(BufferedReader reader, GeneFamilySet geneFamilies) throws IOException, ParseException {
        List<GeneCluster> clusterList = new ArrayList<>();
        GeneClusterBuilder builder = null;
        boolean continueReading = true;
        while (continueReading) {
            String line = reader.readLine().trim();
            switch (line) {
                case GckFileWriter.PARAMETERS_START:
                    readParameters(reader);
                    break;
                case GckFileWriter.CLUSTER_START:
                    if (builder != null) {
                        throw new ParseException("Cluster not closed before new cluster start.", 0);
                    }
                    builder = new GeneClusterBuilder(reader, geneFamilies);
                    builder.readGenes(reader);
                    break;
                case GckFileWriter.OCC_START:
                    if (genomes == null || genomes.length < 0) {
                        throw new ParseException("No genomes read before cluster occ!", 0);
                    }
                    if (builder == null) {
                        throw new ParseException("Not in cluster at occ start!", 0);
                    }
                    if (builder.allOccs != null || builder.bestOccs != null) {
                        throw new ParseException("Multiple cluster occs not supported!", 0);
                    }
                    builder.readOcc(reader, genomes.length);
                    break;
                case GckFileWriter.DCJ_START: // this should'n happen here
                case GckFileWriter.DCJ_END: // this should'n happen here
                    throw new ParseException("DCJ similarity data outside occ section!", 0);
                case GckFileWriter.CLUSTER_END:
                    if (builder == null) {
                        throw new ParseException("Not in cluster at cluster end!", 0);
                    }
                    clusterList.add(builder.build());
                    builder = null;
                    break;
                case GckFileWriter.CLUSTER_SECTION_END:
                    if (builder != null) {
                        throw new ParseException("Cluster not closed before end of cluster section.", 0);
                    }
                    continueReading = false;
                    break;
                default:
                    throw new ParseException("Maleformed line: " + line, 0);
            }
        }
        clusters = clusterList;
    }

    private void readParameters(BufferedReader reader) throws IOException, ParseException {
        if (this.parameters != null) {
            throw new ParseException("More than one parameter section found, only one is allowed!", 0);
        }

        String line = reader.readLine().trim();
        String[] parameters = line.split(GckFileWriter.SEPERATOR);

        Parameter.OperationMode oMode = Parameter.OperationMode.getOperationModeFromChar(parameters[0].charAt(0));
        Parameter.ReferenceType rType = Parameter.ReferenceType.getReferenceTypeFromChar(parameters[1].charAt(0));
        if (!(parameters[2].equals("1") || parameters[2].equals("0"))) {
            throw new ParseException("Invalid value for search ref in ref, must be 0 or 1, got: " + parameters[2], 0);
        }
        boolean searchRefInRef = parameters[2].equals("1");
        int minClusterSize = Integer.parseInt(parameters[3]);
        int quorum = Integer.parseInt(parameters[4]);

        try {
            int delta = Integer.parseInt(parameters[5]);
            this.parameters = new Parameter(delta, minClusterSize, quorum, oMode, rType, searchRefInRef, false);
        } catch (NumberFormatException e) {
            int[][] deltaTable = StringParsing.parseDeltaTable(parameters[5]);
            this.parameters = new Parameter(deltaTable, minClusterSize, quorum, oMode, rType, searchRefInRef, false);
        }

        if (parameters.length > 6) { // to be compatible with previous version files
            double discardedPenality = Double.parseDouble(parameters[6]);
            int borderlineLength = Integer.parseInt(parameters[7]);
            boolean computeSubintervals = parameters[8].equals("1");
            int extendSubsequences = Integer.parseInt(parameters[9]);
            boolean preCompute = parameters[10].equals("1");
            boolean useHeuristics = parameters[11].equals("1");
            this.parameters.setDCJParameters(discardedPenality, borderlineLength, computeSubintervals, extendSubsequences, preCompute, useHeuristics);
        } // if not present, the parameters will be the default values

        line = reader.readLine().trim();
        if (!line.equals(GckFileWriter.PARAMETERS_END)) {
            throw new ParseException("Maleformed line: " + line + "expected " + GckFileWriter.PARAMETERS_END, 0);
        }
    }

    private static class GeneClusterBuilder {

        final int id;
        final int refSeqIndex;
        final Parameter.OperationMode mode;
        final int minTotalDistance;
        final BigDecimal pValue;
        final BigDecimal pValueCorr;
        final double avgDCJSim;
        Set<GeneFamily> genes;
        GeneClusterOccurrence bestOccs;
        GeneClusterOccurrence allOccs;
        private final GeneFamilySet geneFamilies;

        // For each subsequence is mapped a lista of pairs (data of the reference sequence and of the similarity), see RefSeqData class
        private Map<Subsequence, List<AbstractMap.SimpleImmutableEntry<RefSeqData, DCJSimilarityData>>> similarityEntries;

        GeneClusterBuilder(BufferedReader reader, GeneFamilySet geneFamilies) throws IOException, ParseException {
            String line = reader.readLine().trim();
            String[] clusterInfo = line.split(GckFileWriter.SEPERATOR);

            if (clusterInfo.length != 7 && clusterInfo.length != 6) {
                throw new ParseException("Maleformed line: " + line, 0);
            }
            
            // Backward and tests compatibility
            if (clusterInfo.length == 7)
                avgDCJSim = Double.parseDouble(clusterInfo[6]);
            else
                avgDCJSim = Double.NaN;

            id = Integer.parseInt(clusterInfo[0]);
            refSeqIndex = Integer.parseInt(clusterInfo[1]);
            mode = Parameter.OperationMode.getOperationModeFromChar(clusterInfo[2].charAt(0));
            minTotalDistance = Integer.parseInt(clusterInfo[3]);
            pValue = new BigDecimal(clusterInfo[4]);
            pValueCorr = new BigDecimal(clusterInfo[5]);
            this.geneFamilies = geneFamilies;
            similarityEntries = new LinkedHashMap<>(1024);
        }

        private void readGenes(BufferedReader reader) throws IOException, ParseException {
            String line = reader.readLine().trim();
            String[] genes = line.substring(1, line.length() - 1).split(",");
            this.genes = new HashSet<>();
            for (String gene : genes) {
                GeneFamily geneFamily;
                if (gene.trim().equals(GeneFamily.UNKNOWN_GENE_ID)) {
                    geneFamily = geneFamilies.getUnknownGeneFamily();
                } else {
                    geneFamily = geneFamilies.getGeneFamily(gene.trim());
                    if (geneFamily == null) {
                        throw new ParseException("No gene family found for key: " + gene.trim(), 0);
                    }
                }
                this.genes.add(geneFamily);
            }
        }

        private void readOcc(BufferedReader reader, int numberOfGenomes) throws IOException, ParseException {
            boolean continueReading = true;
            int id;
            BigDecimal pValue;
            int totalDist;
            int support;

            String line = reader.readLine().trim();
            String[] occInfo = line.split(GckFileWriter.SEPERATOR);
            if (occInfo.length != 4) {
                throw new ParseException("Maleformed line: " + line, 0);
            }

            id = Integer.parseInt(occInfo[0]);
            pValue = new BigDecimal(occInfo[1]);
            support = Integer.parseInt(occInfo[2]);
            totalDist = Integer.parseInt(occInfo[3]);

            int[] numberOfSubseq = new int[numberOfGenomes];

            List<Subsequence> subsequenceList = new ArrayList<>();
            Subsequence lastSeq = null;

            while (continueReading) {
                line = reader.readLine().trim();
                if (line.equals(GckFileWriter.OCC_END)) {
                    continueReading = false;
                } else if (line.equals(GckFileWriter.DCJ_START)) {
                    readDCJSim(reader, lastSeq);
                } else {
                    String[] sline = line.split(GckFileWriter.SEPERATOR);
                    if (sline.length != 6) {
                        throw new ParseException("Maleformed line: " + line, 0);
                    }
                    lastSeq = new Subsequence(Integer.parseInt(sline[3]), Integer.parseInt(sline[4]), Integer.parseInt(sline[1]), Integer.parseInt(sline[2]), new BigDecimal(sline[5]));
                    subsequenceList.add(lastSeq);
                    numberOfSubseq[Integer.parseInt(sline[0])]++;
                }
            }

            Subsequence[][] subsequences = new Subsequence[numberOfGenomes][];

            int listIndex = 0;
            for (int i = 0; i < subsequences.length; i++) {
                subsequences[i] = new Subsequence[numberOfSubseq[i]];
                for (int j = 0; j < subsequences[i].length; j++) {
                    subsequences[i][j] = subsequenceList.get(listIndex);
                    listIndex++;
                }
            }
            allOccs = new GeneClusterOccurrence(id, subsequences, pValue, totalDist, support);
            bestOccs = allOccs.getBestOccurrence();
        }

        private void readDCJSim(BufferedReader reader, Subsequence lastSeq) throws IOException, ParseException {
            if (lastSeq == null) // dcj tag openned without reading an occ before
            {
                throw new ParseException("DCJ similarity data with no occ before!", 0);
            }

            boolean continueReading = true;

            int refSeqGenome, refSeqChr, refSeqStart, refSeqStop;
            double sim;
            int discarded, cycles, odd, even;
            int startA, stopA, startB, stopB;
            short[] genesMap;

            String line;
            String[] dcj;
            DCJSimilarityData dcjSimData;
            RefSeqData refSeqData;

            while (continueReading) {
                line = reader.readLine().trim();
                if (line.equals(GckFileWriter.DCJ_END)) {
                    continueReading = false;
                } else {
                    dcj = line.split(GckFileWriter.SEPERATOR);
                    if (dcj.length != 13 && dcj.length != 14) {
                        throw new ParseException("Maleformed line: " + line, 0);
                    }

                    // These are used to store the data in a temporary data structure until we have finished reading all gene cluster occurrences
                    refSeqGenome = Integer.parseInt(dcj[0]);
                    refSeqChr = Integer.parseInt(dcj[1]);
                    refSeqStart = Integer.parseInt(dcj[2]);
                    refSeqStop = Integer.parseInt(dcj[3]);

                    // These are given to the DCJSimilarityData constructor
                    sim = Double.parseDouble(dcj[4]);
                    discarded = Integer.parseInt(dcj[5]);
                    cycles = Integer.parseInt(dcj[6]);
                    odd = Integer.parseInt(dcj[7]);
                    even = Integer.parseInt(dcj[8]);
                    startA = Integer.parseInt(dcj[9]);
                    stopA = Integer.parseInt(dcj[10]);
                    startB = Integer.parseInt(dcj[11]);
                    stopB = Integer.parseInt(dcj[12]);
                    genesMap = dcj.length == 14 ? parseStrToShortArray(dcj[13]) : null;
                    
                    refSeqData = new RefSeqData(refSeqGenome, refSeqChr, refSeqStart, refSeqStop);
                    dcjSimData = new DCJSimilarityData(sim, discarded, null, cycles, odd, even, startA, stopA, startB, stopB, genesMap);
                    addSimilarityEntry(lastSeq, refSeqData, dcjSimData);
                }
            }
        }

        private void addSimilarityEntry(Subsequence seq, RefSeqData refSeqData, DCJSimilarityData dcjSimData) {
            List<AbstractMap.SimpleImmutableEntry<RefSeqData, DCJSimilarityData>> list;

            list = similarityEntries.get(seq);
            if (list == null) {
                list = new LinkedList<>();
                similarityEntries.put(seq, list);
            }

            list.add(new AbstractMap.SimpleImmutableEntry<>(refSeqData, dcjSimData));
        }

        private Subsequence findRefSeq(RefSeqData refSeqData) throws ParseException {
            // allOccs.getSubsequences()[i] contains the subsequences in the genome i
            Subsequence[] subsequencesInGenome = allOccs.getSubsequences()[refSeqData.getGenome()];
            for (Subsequence s : subsequencesInGenome) {
                if (s.getChromosome() == refSeqData.getChromosome()
                        && s.getStart() == refSeqData.getStart()
                        && s.getStop() == refSeqData.getStop()) {
                    return s;
                }
            }
            throw new ParseException("Missing reference sequence! " + refSeqData, 0);
        }

        GeneCluster build() throws ParseException {
            if (genes == null) {
                throw new ParseException("Missing genes when trying to complete cluster!", 0);
            }
            if (allOccs == null || bestOccs == null) {
                throw new ParseException("Missing occs when trying to complete cluster!", 0);
            }
            /*
            System.out.println("Subsequences read:");
            int i = 0;
            for (Subsequence[] setS : allOccs.getSubsequences()) {
                int j = 0;
                for (Subsequence s : setS)
                    System.out.println(i + "," + j++ + ": " + s);
                i++;
            }
             */

            // Set DCJ similarity data
            List<AbstractMap.SimpleImmutableEntry<RefSeqData, DCJSimilarityData>> simEntriesForThisSeq;
            for (Subsequence seq : similarityEntries.keySet()) {
                simEntriesForThisSeq = similarityEntries.get(seq);
                //System.out.println("Seq: " + seq);
                for (AbstractMap.SimpleImmutableEntry<RefSeqData, DCJSimilarityData> entry : simEntriesForThisSeq) {
                    RefSeqData refSeqData = entry.getKey();
                    DCJSimilarityData dcjSimData = entry.getValue();
                    Subsequence refseq = findRefSeq(refSeqData);
                    seq.addDCJSim(refseq, dcjSimData);
                    //System.out.println("\tRefSeq: " + refseq + ", dcjSimData: " + dcjSimData.toString(false));
                }
            }

            return new GeneCluster(id,
                    bestOccs,
                    allOccs,
                    genes,
                    pValue,
                    pValueCorr,
                    minTotalDistance,
                    refSeqIndex,
                    mode,
                    avgDCJSim);
        }

        /*
         * Class used to store data of one reference sequence. We store this
         * data in this simplified way until we have finished reading all gene
         * cluster occurrences/sequences, then we can refer one gene sequence as
         * reference to another sequence.
         */
        private static class RefSeqData {

            private final int genome;
            private final int chromosome;
            private final int start;
            private final int stop;

            public RefSeqData(int genome, int chromosome, int start, int stop) {
                this.genome = genome;
                this.chromosome = chromosome;
                this.start = start;
                this.stop = stop;
            }

            public int getGenome() {
                return genome;
            }

            public int getChromosome() {
                return chromosome;
            }

            public int getStart() {
                return start;
            }

            public int getStop() {
                return stop;
            }

            @Override
            public String toString() {
                return "RefSeqData{" + "genome=" + genome + ", chromosome=" + chromosome + ", start=" + start + ", stop=" + stop + '}';
            }
        }
    }

    /**
     * Method for handling errors while the file is read.
     */
    private void handleFailedSessionLoad() {
        genomes = null;
        geneFamilies = null;
        clusters = null;
        maxIdLength = 0;
        maxLocusTagLength = 0;
        maxNameLength = 0;
    }
}
