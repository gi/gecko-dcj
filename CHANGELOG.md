# Changelog

## up to [3.1.dcj.4] - 2019-07-26

### Added
- Everything related to the local DCJ similarity
- Session file in compressed format (.gckz)
- Script for generating a .cog file (gecko input) given species .gbk files
- Additional results filter in the graphical user interface ("showFiltered+")

### Changed
- Many small tweeks and improvements in the interface

### Fixed
- A couple of small bugs fixed in the graphical user interface

